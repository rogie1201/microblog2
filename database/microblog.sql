-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 08, 2021 at 10:56 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `microblog`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(20) NOT NULL,
  `post_id` int(20) NOT NULL,
  `user_id` int(20) NOT NULL,
  `comment_text` varchar(255) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `modified` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `post_id`, `user_id`, `comment_text`, `is_deleted`, `created`, `modified`, `deleted`) VALUES
(1, 17, 10, 'test', 0, '2021-03-04 01:26:26', '2021-03-04 01:26:26', NULL),
(2, 17, 10, '2nd comment', 0, '2021-03-04 01:27:49', '2021-03-04 01:27:49', NULL),
(3, 17, 10, '3rd comment', 0, '2021-03-04 01:29:05', '2021-03-04 01:29:05', NULL),
(4, 17, 10, '4th Comment', 0, '2021-03-04 09:32:03', '2021-03-04 09:32:03', NULL),
(5, 17, 10, '5th comment', 0, '2021-03-04 09:33:12', '2021-03-04 09:33:12', NULL),
(6, 17, 10, '6th comment', 0, '2021-03-04 09:34:58', '2021-03-04 09:34:58', NULL),
(7, 17, 10, '7th comment', 0, '2021-03-04 09:35:43', '2021-03-04 09:35:43', NULL),
(8, 17, 10, '8th comment', 0, '2021-03-04 09:39:21', '2021-03-04 09:39:21', NULL),
(9, 17, 10, '9th comment', 0, '2021-03-04 09:40:42', '2021-03-04 09:40:42', NULL),
(10, 17, 10, '10th comment', 0, '2021-03-04 09:48:37', '2021-03-04 09:48:37', NULL),
(11, 17, 10, '11th comment', 0, '2021-03-04 09:49:13', '2021-03-04 09:49:13', NULL),
(12, 17, 10, '12th comment', 0, '2021-03-04 10:05:56', '2021-03-04 10:05:56', NULL),
(13, 17, 10, '13th comment', 0, '2021-03-04 10:06:56', '2021-03-04 10:06:56', NULL),
(14, 17, 10, '14th comment', 0, '2021-03-04 10:10:36', '2021-03-04 10:10:36', NULL),
(15, 17, 10, '15th comment', 0, '2021-03-04 10:11:31', '2021-03-04 10:11:31', NULL),
(16, 17, 10, '16th comment', 0, '2021-03-04 10:12:29', '2021-03-04 10:12:29', NULL),
(17, 17, 10, '17th comment', 0, '2021-03-04 10:31:53', '2021-03-04 10:31:53', NULL),
(18, 17, 10, '18th comment', 0, '2021-03-04 10:33:00', '2021-03-04 10:33:00', NULL),
(19, 17, 40, 'My comment', 1, '2021-03-04 10:36:13', '2021-03-04 10:49:54', '2021-03-04 10:49:54'),
(20, 13, 10, 'Scrum is flexible ', 1, '2021-03-04 11:41:01', '2021-03-04 11:43:07', '2021-03-04 11:43:07'),
(21, 13, 10, 'nice', 1, '2021-03-04 11:42:48', '2021-03-04 11:43:02', '2021-03-04 11:43:02'),
(22, 13, 10, 'Agile Framework', 0, '2021-03-04 11:43:39', '2021-03-04 11:43:39', NULL),
(23, 13, 10, 'Sprint or Modules', 0, '2021-03-04 11:44:19', '2021-03-04 11:44:19', NULL),
(24, 13, 10, 'test101', 0, '2021-03-04 11:55:12', '2021-03-04 11:55:12', NULL),
(25, 13, 10, 'test102', 0, '2021-03-04 11:55:58', '2021-03-04 11:55:58', NULL),
(26, 13, 10, 'test103', 0, '2021-03-04 11:56:21', '2021-03-04 11:56:21', NULL),
(27, 13, 10, 'test104', 0, '2021-03-04 12:00:14', '2021-03-04 12:00:14', NULL),
(28, 13, 10, 'test 105', 0, '2021-03-04 12:02:05', '2021-03-04 12:02:05', NULL),
(29, 13, 10, 'test 106', 0, '2021-03-04 12:03:40', '2021-03-04 12:03:40', NULL),
(30, 13, 10, 'test 107', 0, '2021-03-04 12:07:29', '2021-03-04 12:07:29', NULL),
(31, 13, 10, 'test 108', 0, '2021-03-04 12:12:08', '2021-03-04 12:12:08', NULL),
(32, 13, 10, 'test 109', 0, '2021-03-04 12:13:15', '2021-03-04 12:13:15', NULL),
(33, 13, 10, 'test110', 0, '2021-03-04 13:01:21', '2021-03-04 13:01:21', NULL),
(34, 13, 10, 'test111', 0, '2021-03-04 13:02:57', '2021-03-04 13:02:57', NULL),
(35, 13, 10, 'test112', 0, '2021-03-04 13:04:46', '2021-03-04 13:04:50', '2021-03-04 13:04:50'),
(36, 13, 10, 'test 113', 0, '2021-03-04 13:07:56', '2021-03-04 13:07:59', '2021-03-04 13:07:59'),
(37, 13, 10, 'test 114 ', 0, '2021-03-04 13:10:05', '2021-03-04 13:10:05', NULL),
(38, 13, 10, 'test 115', 1, '2021-03-04 13:10:55', '2021-03-04 13:15:11', '2021-03-04 13:15:11'),
(39, 13, 10, 'test 116', 1, '2021-03-04 13:11:28', '2021-03-04 13:14:30', '2021-03-04 13:14:30'),
(40, 13, 10, 'test 117', 1, '2021-03-04 13:12:26', '2021-03-04 13:14:02', '2021-03-04 13:14:02'),
(41, 13, 10, 'test 118', 1, '2021-03-04 13:13:15', '2021-03-04 13:13:59', '2021-03-04 13:13:59'),
(42, 13, 10, 'test 119', 1, '2021-03-04 13:13:48', '2021-03-04 13:13:52', '2021-03-04 13:13:52'),
(43, 4, 10, 'I remember my first post :D', 0, '2021-03-04 13:21:54', '2021-03-04 13:21:54', NULL),
(44, 13, 10, 'XSS test: <script>alert(\"Hacked!\");</script>', 0, '2021-03-04 14:21:25', '2021-03-04 14:21:25', NULL),
(45, 25, 10, 'I shared my post', 0, '2021-03-05 10:16:09', '2021-03-05 10:16:09', NULL),
(46, 25, 40, 'for real', 0, '2021-03-05 13:17:47', '2021-03-05 13:17:47', NULL),
(47, 23, 10, 'it\'s amazing  men.', 0, '2021-03-05 17:23:11', '2021-03-05 17:23:11', NULL),
(48, 15, 10, 'Nice bruh :D', 0, '2021-03-05 17:23:47', '2021-03-05 17:23:47', NULL),
(49, 23, 40, 'Yeah thanks :D', 0, '2021-03-08 09:41:02', '2021-03-08 09:41:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `follows`
--

CREATE TABLE `follows` (
  `id` int(20) NOT NULL,
  `user_id` int(20) NOT NULL,
  `follower_user_id` int(20) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `modified` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `follows`
--

INSERT INTO `follows` (`id`, `user_id`, `follower_user_id`, `is_deleted`, `created`, `modified`, `deleted`) VALUES
(3, 42, 10, 0, '2021-02-24 12:25:09', '2021-03-04 17:40:36', '2021-03-04 17:40:17'),
(10, 10, 40, 0, '2021-02-26 09:05:33', '2021-02-26 09:05:33', NULL),
(13, 44, 10, 0, '2021-03-02 01:12:52', '2021-03-03 13:15:46', '2021-03-03 13:15:46'),
(14, 10, 41, 0, '2021-03-02 17:17:08', '2021-03-02 17:17:08', NULL),
(15, 40, 10, 0, '2021-03-03 13:16:15', '2021-03-04 17:40:54', '2021-03-04 17:40:52'),
(16, 10, 43, 0, '2021-03-03 13:17:13', '2021-03-03 13:18:10', '2021-03-03 13:18:10'),
(21, 43, 10, 0, '2021-03-03 14:02:39', '2021-03-03 14:03:39', '2021-03-03 14:03:22');

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `id` int(20) NOT NULL,
  `post_id` int(20) NOT NULL,
  `user_id` int(20) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `modified` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`id`, `post_id`, `user_id`, `is_deleted`, `created`, `modified`, `deleted`) VALUES
(1, 4, 10, 0, '2021-03-03 10:58:47', '2021-03-04 13:22:04', '2021-03-04 13:22:03'),
(3, 4, 44, 0, '2021-03-03 10:58:47', NULL, NULL),
(4, 5, 10, 0, '2021-03-03 14:55:44', '2021-03-03 14:55:44', NULL),
(6, 6, 10, 0, '2021-03-03 16:03:58', '2021-03-03 17:05:47', '2021-03-03 16:35:33'),
(7, 9, 10, 1, '2021-03-03 16:34:48', '2021-03-03 16:35:31', '2021-03-03 16:35:31'),
(8, 10, 10, 1, '2021-03-03 16:34:50', '2021-03-03 16:35:16', '2021-03-03 16:35:16'),
(9, 8, 10, 1, '2021-03-03 16:39:04', '2021-03-03 16:39:06', '2021-03-03 16:39:06'),
(10, 17, 10, 0, '2021-03-03 16:59:31', '2021-03-04 17:41:08', '2021-03-04 17:41:08'),
(11, 23, 10, 0, '2021-03-03 17:17:59', '2021-03-03 18:25:09', '2021-03-03 18:25:03'),
(12, 17, 40, 0, '2021-03-04 11:25:20', '2021-03-05 17:25:53', '2021-03-04 11:26:33'),
(13, 13, 10, 0, '2021-03-04 13:02:45', '2021-03-05 08:39:59', '2021-03-05 08:39:58'),
(14, 25, 10, 1, '2021-03-05 16:01:04', '2021-03-05 16:01:11', '2021-03-05 16:01:11'),
(15, 8, 40, 0, '2021-03-05 17:25:35', '2021-03-05 17:25:35', NULL),
(16, 7, 40, 0, '2021-03-05 17:25:38', '2021-03-05 17:25:46', '2021-03-05 17:25:45'),
(17, 15, 40, 0, '2021-03-05 17:46:40', '2021-03-05 17:46:40', NULL),
(18, 23, 40, 0, '2021-03-08 09:41:18', '2021-03-08 10:15:53', '2021-03-08 10:15:53');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(20) NOT NULL,
  `repost_id` int(20) DEFAULT NULL,
  `user_id` int(20) NOT NULL,
  `content` varchar(250) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `modified` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `repost_id`, `user_id`, `content`, `image`, `is_deleted`, `created`, `modified`, `deleted`) VALUES
(4, NULL, 10, 'first post', '4.jpg', 0, '2021-02-22 06:09:59', '2021-02-22 06:09:59', NULL),
(5, NULL, 10, '2nd post', '5.jpg', 0, '2021-02-22 06:11:25', '2021-02-22 06:11:25', NULL),
(6, NULL, 10, '3rd post', NULL, 0, '2021-02-22 06:13:10', '2021-02-22 06:13:10', NULL),
(7, NULL, 10, 'test', NULL, 0, '2021-02-22 06:15:48', '2021-02-26 13:00:52', NULL),
(8, NULL, 10, 'The quick brown fox jump over the lazy dog.', NULL, 0, '2021-02-22 06:18:52', '2021-03-03 14:46:50', '2021-02-26 13:40:05'),
(9, NULL, 10, 'noice', '9.gif', 0, '2021-02-22 06:24:35', '2021-02-22 06:24:35', NULL),
(10, NULL, 10, 'This is my post for today \"The Joke\" :D', '10.gif', 0, '2021-02-22 07:01:46', '2021-02-22 07:01:46', NULL),
(13, NULL, 10, 'My post of the day', '13.jpg', 0, '2021-02-23 10:58:48', '2021-03-03 22:03:03', '2021-02-26 11:39:21'),
(14, 13, 44, 'My post of the day', '13.jpg', 0, '2021-02-23 12:58:48', '2021-02-26 11:50:10', '2021-02-26 11:50:10'),
(15, NULL, 40, 'This is for her poster making', '151614935997.jpg', 0, '2021-02-23 13:03:03', '2021-03-05 17:19:57', NULL),
(17, NULL, 40, 'The minions :D :D :D', '171614075424.jpg', 0, '2021-02-23 16:40:08', '2021-02-23 19:59:55', '2021-02-23 18:17:36'),
(20, NULL, 10, 'https://9gag.com/', NULL, 1, '2021-02-24 08:35:09', '2021-02-24 08:37:14', '2021-02-24 08:37:14'),
(23, 15, 10, 'This is for her poster making', '151614064000.jpg', 0, '2021-03-03 17:12:44', '2021-03-03 17:12:44', NULL),
(24, NULL, 10, 'Post XSS test: <script>alert(\"Hacked!\");</script>', NULL, 0, '2021-03-04 14:24:53', '2021-03-04 14:24:53', NULL),
(25, 24, 10, 'Post XSS test: <script>alert(\"Hacked!\");</script>', '', 0, '2021-03-05 10:15:32', '2021-03-05 10:15:32', NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `post_views`
-- (See below for the actual view)
--
CREATE TABLE `post_views` (
`POST_ID` int(20)
,`USER_ID` int(20)
,`USERNAME` varchar(225)
,`FULL_NAME` varchar(225)
,`USER_IMAGE` varchar(225)
,`IMAGE` varchar(250)
,`CONTENT` varchar(250)
,`GROUP_ID` int(20)
,`POST_TYPE` varchar(12)
,`ORGINAL_USERNAME` varchar(225)
,`ORG_NAME` varchar(225)
,`ORG_USER_IMAGE` varchar(225)
,`ORG_MODIFIED` datetime
,`ORG_USER_ID` int(20)
,`IS_DELETED` tinyint(4)
,`MODIFIED` datetime
,`LIKES_COUNT` bigint(21)
,`COMMENTS_COUNT` bigint(21)
,`IS_LIKED` int(1)
);

-- --------------------------------------------------------

--
-- Table structure for table `reposts`
--

CREATE TABLE `reposts` (
  `id` int(20) NOT NULL,
  `post_id` int(20) NOT NULL,
  `user_id` int(20) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `modified` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `reposts`
--

INSERT INTO `reposts` (`id`, `post_id`, `user_id`, `is_deleted`, `created`, `modified`, `deleted`) VALUES
(4, 8, 10, 0, '2021-02-26 12:44:55', '2021-02-26 12:44:55', NULL),
(7, 7, 10, 0, '2021-02-26 13:00:52', '2021-02-26 13:00:52', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(20) NOT NULL,
  `full_name` varchar(225) NOT NULL,
  `email` varchar(225) NOT NULL,
  `birthdate` date NOT NULL,
  `username` varchar(225) NOT NULL,
  `password` varchar(225) NOT NULL,
  `image` varchar(225) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `code_of_activation` varchar(225) DEFAULT NULL,
  `email_activated` datetime DEFAULT NULL,
  `created` datetime DEFAULT current_timestamp(),
  `modified` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `full_name`, `email`, `birthdate`, `username`, `password`, `image`, `is_deleted`, `code_of_activation`, `email_activated`, `created`, `modified`, `deleted`) VALUES
(10, 'bill gates', '101gates@gmail.com', '2021-02-01', 'gates', '$2a$10$kbv4vE4NYlItVGm5u042..GJU7lD5tm5uoB.q/v54HRC8CbgpIg9q', '101614909436.png', 0, '-', '2021-02-19 11:50:59', '2021-02-18 09:23:46', '2021-03-05 09:57:19', NULL),
(40, 'Steve J Rogers', 'zteverogers@gmail.com', '2021-02-01', 'steve', '$2a$10$qshCg9xeSdQQD5B497k14uIgsf1Gf5y6w7TlxMh1EZnGxg5ptx7Rm', '401614909371.gif', 0, '-', '2021-02-23 13:01:05', '2021-02-19 02:57:28', '2021-03-08 09:40:24', NULL),
(41, 'Jeff Bezos', 'jeffbezos760@gmail.com', '1995-02-01', 'jeffbezos760', '$2a$10$WFjwmbIscu1m92Sg8HaApul03Ztl6rW/B6O0qVKHnel7UlkHVRwgW', 'default-profile.jpg', 0, '-', '2021-02-23 22:58:14', '2021-02-23 22:57:10', '2021-02-23 22:58:14', NULL),
(42, 'Elon Musk', 'emusk4942@gmail.com', '1994-02-01', 'emusk4942', '$2a$10$7aS.Uii.uf8xnhcCKIxkFutjDr7eExX./tvcXqJf2sPezFNNRTDyK', 'default-profile.jpg', 0, '-', '2021-02-23 23:13:17', '2021-02-23 23:13:03', '2021-02-23 23:13:17', NULL),
(43, 'Mark Zuckerberg', 'markzuckerbergz990@gmail.com', '1992-01-01', 'markzuckerbergz990', '$2a$10$NS9YSTPDJJXv2ezQUvRaKeH4c0VbpFfgfeyh0z1tdda7aQ1xfHbSm', 'default-profile.jpg', 0, '-', '2021-02-23 23:19:41', '2021-02-23 23:19:09', '2021-02-23 23:19:41', NULL),
(44, 'Larry Page', 'larrypage@gmail.com', '1990-01-01', 'larrypage', '$2a$10$LFLbCt/0infk4WAl/BWATOLvc3znJJ8JoG/1U9o8r6U0lFH8GUOra', 'default-profile.jpg', 0, '-', '2021-02-23 23:26:23', '2021-02-23 23:23:56', '2021-02-23 23:23:56', NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `user_post_views`
-- (See below for the actual view)
--
CREATE TABLE `user_post_views` (
`POST_ID` int(20)
,`USER_ID` int(20)
,`USERNAME` varchar(225)
,`FULL_NAME` varchar(225)
,`USER_IMAGE` varchar(225)
,`IMAGE` varchar(250)
,`CONTENT` varchar(250)
,`GROUP_ID` int(20)
,`POST_TYPE` varchar(12)
,`ORGINAL_USERNAME` varchar(225)
,`ORG_NAME` varchar(225)
,`ORG_USER_IMAGE` varchar(225)
,`ORG_MODIFIED` datetime
,`ORG_USER_ID` int(20)
,`IS_DELETED` tinyint(4)
,`MODIFIED` datetime
,`LIKES_COUNT` bigint(21)
,`COMMENTS_COUNT` bigint(21)
,`IS_LIKED` int(1)
);

-- --------------------------------------------------------

--
-- Structure for view `post_views`
--
DROP TABLE IF EXISTS `post_views`;

CREATE  VIEW `post_views`  AS 

SELECT 
ME_POST.ID POST_ID 
, 0 as IS_DELETED_SHARED
,ME.ID USER_ID
,ME.USERNAME
,ME.FULL_NAME
,ME.IMAGE USER_IMAGE
,ME_POST.IMAGE
, ME_POST.CONTENT
, ME.ID GROUP_ID
, 'REGULAR_POST' POST_TYPE
, NULL ORGINAL_USERNAME
, NULL ORG_NAME
, NULL ORG_USER_IMAGE
, NULL ORG_MODIFIED
, NULL ORG_USER_ID
, ME_POST.IS_DELETED
,ME_POST.MODIFIED 
,(select count(*) from likes where POST_ID = ME_POST.ID and is_deleted = 0) as LIKES_COUNT
,(select count(*) from comments where POST_ID = ME_POST.ID and is_deleted = 0) as COMMENTS_COUNT
,((CASE WHEN ME.ID = (SELECT b.user_id FROM likes as b where user_id =  ME.ID and post_id = ME_POST.ID and is_deleted = 0 ) THEN 1 ELSE 0 END)) AS IS_LIKED
FROM users ME
LEFT JOIN posts ME_POST ON ME_POST.USER_ID= ME.ID
WHERE 
ME_POST.repost_id IS NULL
AND ME_POST.is_deleted != 1

union all


SELECT 
ME_POST.ID  
, 0 as IS_DELETED_SHARED
,THEM.ID USER_ID
,THEM.USERNAME
,THEM.FULL_NAME
,THEM.IMAGE USER_IMAGE
,ME_POST.IMAGE
, ME_POST.CONTENT
, FOL.FOLLOWER_USER_ID GROUP_ID
, 'REGULAR_POST' post_type
, NULL ORG_USERNAME
, NULL ORG_NAME
, NULL ORG_USER_IMAGE
, NULL ORG_MODIFIED
, NULL ORG_USER_ID
, ME_POST.IS_DELETED
,ME_POST.modified 
,(select count(*) from likes where POST_ID = ME_POST.ID and is_deleted = 0) as LIKES_COUNT
,(select count(*) from comments where POST_ID = ME_POST.ID and is_deleted = 0) as COMMENTS_COUNT
,((CASE WHEN FOL.FOLLOWER_USER_ID = (SELECT b.user_id FROM likes as b where user_id =  FOL.FOLLOWER_USER_ID and post_id = ME_POST.ID and is_deleted = 0) THEN 1 ELSE 0 END)) AS IS_LIKED
FROM follows FOL
LEFT JOIN users THEM ON THEM.id = FOL.USER_ID
LEFT JOIN posts ME_POST ON ME_POST.USER_ID= THEM.ID
WHERE 
ME_POST.REPOST_ID IS NULL
AND ME_POST.is_deleted != 1
AND FOL.is_deleted != 1

union all

 
SELECT 
ME_POST.ID
, ME_POST.IS_DELETED as IS_DELETED_SHARED
,ME.ID USER_ID
,ME.USERNAME
,ME.FULL_NAME
,ME.IMAGE USER_IMAGE
, SHARED_POST.IMAGE
, SHARED_POST.CONTENT
, ME.ID GROUP_ID
, 'SHARED_POST' post_type
, ORG_USER.USERNAME ORG_USERNAME
, ORG_USER.FULL_NAME ORG_NAME
, ORG_USER.IMAGE ORG_USER_IMAGE
, SHARED_POST.modified ORG_MODIFIED
, ORG_USER.ID ORG_USER_ID
, SHARED_POST.IS_DELETED
, ME_POST.modified
,(select count(*) from likes where POST_ID = ME_POST.ID and is_deleted = 0) as LIKES_COUNT
,(select count(*) from comments where POST_ID = ME_POST.ID and is_deleted = 0) as COMMENTS_COUNT
,((CASE WHEN ME.ID = (SELECT b.user_id FROM likes as b where user_id = ME.ID and post_id = ME_POST.ID and is_deleted = 0) THEN 1 ELSE 0 END)) AS IS_LIKED
FROM users ME
LEFT JOIN posts ME_POST ON ME_POST.USER_ID= ME.ID
INNER JOIN posts SHARED_POST ON ME_POST.REPOST_ID = SHARED_POST.ID AND SHARED_POST.IS_DELETED = 0
LEFT JOIN users ORG_USER ON ORG_USER.ID = SHARED_POST.USER_ID

union all


SELECT 
ME_POST.ID
, ME_POST.IS_DELETED as IS_DELETED_SHARED
,THEM.ID USER_ID
,THEM.USERNAME
,THEM.FULL_NAME
,THEM.IMAGE USER_IMAGE
, SHARED_POST.IMAGE
, SHARED_POST.CONTENT
, FOL.USER_ID GROUP_ID
, 'SHARED_POST' post_type
, ORG_USER.USERNAME ORG_USERNAME
, ORG_USER.FULL_NAME ORG_NAME
, ORG_USER.IMAGE ORG_USER_IMAGE
, SHARED_POST.modified ORG_MODIFIED
, ORG_USER.ID ORG_USER_ID
, SHARED_POST.IS_DELETED
, ME_POST.modified
,(select count(*) from likes where POST_ID = ME_POST.ID and is_deleted = 0) as LIKES_COUNT
,(select count(*) from comments where POST_ID = ME_POST.ID and is_deleted = 0) as COMMENTS_COUNT
,((CASE WHEN FOL.USER_ID = (SELECT b.user_id FROM likes as b where user_id = FOL.USER_ID and post_id = ME_POST.ID and is_deleted = 0) THEN 1 ELSE 0 END)) AS IS_LIKED
FROM follows FOL
LEFT JOIN users THEM ON THEM.ID = FOL.FOLLOWER_USER_ID
LEFT JOIN posts ME_POST ON ME_POST.USER_ID = FOL.FOLLOWER_USER_ID
INNER JOIN posts SHARED_POST ON ME_POST.REPOST_ID = SHARED_POST.ID
LEFT JOIN users ORG_USER ON ORG_USER.ID = SHARED_POST.USER_ID
WHERE 
ME_POST.REPOST_ID IS NOT NULL
AND FOL.IS_DELETED != 1;


-- --------------------------------------------------------

--
-- Structure for view `user_post_views`
--
DROP TABLE IF EXISTS `user_post_views`;

CREATE VIEW `user_post_views` AS 


SELECT 
ME_POST.ID POST_ID 
,0 as IS_DELETED_SHARED
,ME.ID USER_ID
,ME.USERNAME
,ME.FULL_NAME
,ME.IMAGE USER_IMAGE
,ME_POST.IMAGE
, ME_POST.CONTENT
, ME.ID GROUP_ID
, 'REGULAR_POST' POST_TYPE
, NULL ORGINAL_USERNAME
, NULL ORG_NAME
, NULL ORG_USER_IMAGE
, NULL ORG_MODIFIED
, NULL ORG_USER_ID
, ME_POST.IS_DELETED
,ME_POST.MODIFIED 
,(select count(*) from likes where POST_ID = ME_POST.ID and is_deleted = 0) as LIKES_COUNT
,(select count(*) from comments where POST_ID = ME_POST.ID and is_deleted = 0) as COMMENTS_COUNT
,((CASE WHEN ME.ID = (SELECT b.user_id FROM likes as b where user_id =  ME.ID and post_id = ME_POST.ID and is_deleted = 0 ) THEN 1 ELSE 0 END)) AS IS_LIKED
FROM users ME
LEFT JOIN posts ME_POST ON ME_POST.USER_ID= ME.ID
WHERE 
ME_POST.repost_id IS NULL
AND ME_POST.is_deleted != 1

union all

 
SELECT 
ME_POST.ID
, ME_POST.IS_DELETED as IS_DELETED_SHARED
,ME.ID USER_ID
,ME.USERNAME
,ME.FULL_NAME
,ME.IMAGE USER_IMAGE
, SHARED_POST.IMAGE
, SHARED_POST.CONTENT
, ME.ID GROUP_ID
, 'SHARED_POST' post_type
, ORG_USER.USERNAME ORG_USERNAME
, ORG_USER.FULL_NAME ORG_NAME
, ORG_USER.IMAGE ORG_USER_IMAGE
, SHARED_POST.modified ORG_MODIFIED
, ORG_USER.ID ORG_USER_ID
, SHARED_POST.IS_DELETED
, ME_POST.modified
,(select count(*) from likes where POST_ID = ME_POST.ID and is_deleted = 0) as LIKES_COUNT
,(select count(*) from comments where POST_ID = ME_POST.ID and is_deleted = 0) as COMMENTS_COUNT
,((CASE WHEN ME.ID = (SELECT b.user_id FROM likes as b where user_id = ME.ID and post_id = ME_POST.ID and is_deleted = 0) THEN 1 ELSE 0 END)) AS IS_LIKED
FROM users ME
LEFT JOIN posts ME_POST ON ME_POST.USER_ID= ME.ID
INNER JOIN posts SHARED_POST ON ME_POST.REPOST_ID = SHARED_POST.ID
LEFT JOIN users ORG_USER ON ORG_USER.ID = SHARED_POST.USER_ID;


--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `follows`
--
ALTER TABLE `follows`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reposts`
--
ALTER TABLE `reposts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `follows`
--
ALTER TABLE `follows`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `reposts`
--
ALTER TABLE `reposts`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
