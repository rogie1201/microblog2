<?php
// app/Controller/UsersController.php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
App::uses('CakeTime', 'Utility');
App::uses('DboSource', 'Model/DataSource');

/**
 * Handles logic that has something to do with Users.
 *
 * @category Users
 * @package  AppModel
 * @author   Rogie Mar <rmmahilum@gmail.com>
 * @license  Free https://bitbucket.org/rogie1201/microblog2/src/master/
 * @link     None
 */
class UsersController extends AppController
{

    public $Helpers = array('Session', 'Html');

    /**
     * User can access the view without authentication
     *
     * @return view
     */
    public function beforeFilter()
    {
        parent::beforeFilter();
        // Allow users to register and logout.
        $this->Auth->allow('add', 'logout', 'activate', 'forgotPassword', 'resetPassword');
    }

    /**
     * Check if the user is authorized.
     *
     * @param int $user The user id.
     *
     * @return boolean true(If owned by the user) or false(not owned)
     */
    public function isAuthorized($user)
    {
        if (in_array($this->action, array('index', 'view', 'follow', 'unfollow', 'userToFollow', 'edit'))) {
            return true;
        }
        if (in_array($this->action, array('/', 'edit'))) {
            $userId = (int)$this->request->params['pass'][0];
            if ($this->User->isOwnedBy($userId)) {

                return true;
            }
        }
        return false;
    }

    /**
     * Display the details of the logged in user and its posts.
     *
     * @return array posts, followers, followings
     */
    public function index()
    {

        $this->loadModel('Follow');
        $this->loadModel('Post');
        $this->loadModel('User');
        $this->loadModel('UserPostView');

        //$this->set('posts', $this->UserPostView->getFeedPostPerUser($this->Auth->user('id')));

        $this->paginate = array(
            'conditions' => array('UserPostView.GROUP_ID' => $this->Auth->user('id'),
                'UserPostView.IS_DELETED_SHARED !=' => 1),
            'order' => 'UserPostView.MODIFIED DESC',
            'limit' => 5,
        );

        $this->set('posts', $this->paginate('UserPostView'));

        $followers = $this->Follow->getFollowers($this->Auth->user('id'));
        $followings = $this->Follow->getFollowings($this->Auth->user('id'));
        $tofollow = $this->User->getUserToFollow($this->Auth->user('id'));

        $this->set('followers', $followers);
        $this->set('followings', $followings);
        $this->set('follows', $tofollow);

        $followers_view = $this->Follow->getFollowers($this->Auth->user('id'));
        $followings_view = $this->Follow->getFollowings($this->Auth->user('id'));

        $this->set('followers_view', $followers_view);
        $this->set('followings_view', $followings_view);

    }

    /**
     * View the details of the user.
     *
     * @param int $id The user id
     *
     * @return array posts, followers, followings
     */
    public function view($id = null)
    {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            $this->Flash->default('Invalid user');
            return $this->redirect($this->Auth->logout());
        } else {

            if (!empty($this->Auth->user('id'))) {

                $this->loadModel('Follow');
                $this->loadModel('Post');
                $this->loadModel('User');
                $this->loadModel('UserPostView');

                $this->set('posts', $this->UserPostView->getFeedPostPerUser($id));

                $this->paginate = array(
                    'conditions' => array('UserPostView.GROUP_ID' => $id,
                        'UserPostView.IS_DELETED_SHARED !=' => 1),
                    'order' => 'UserPostView.MODIFIED DESC',
                    'limit' => 5,
                );

                $this->set('posts', $this->paginate('UserPostView'));

                $followers = $this->Follow->getFollowers($id);
                $followings = $this->Follow->getFollowings($id);
                $tofollow = $this->User->getUserToFollow($this->Auth->user('id'));

                $this->set('followers', $followers);
                $this->set('followings', $followings);
                $this->set('follows', $tofollow);

                $userData = $this->User->find('first', array(
                    'conditions' => array(
                        'User.id' => $id,
                        'User.email_activated IS NOT NULL',
                    ),
                ));

                $this->set('userData', $userData);

                $followers_view = $this->Follow->getFollowers($this->Auth->user('id'));
                $followings_view = $this->Follow->getFollowings($this->Auth->user('id'));

                $this->set('followers_view', $followers_view);
                $this->set('followings_view', $followings_view);

                $isfollower = $this->Follow->getFollowers($this->Auth->user('id'));
                $this->set('isfollower', $isfollower);
            } else {
                return $this->redirect($this->Auth->logout());
            }

        }

    }

    /**
     * Add a user.
     *
     * Function will send email activation code to the user's email
     *
     * @return string user error exeption
     * @throws Exception If something goes wrong.
     */
    public function add()
    {
        if ($this->request->is('post')) {
            $this->User->create();

            $this->request->data['User']['code_of_activation'] = $activation_key = time();

            if ($this->User->save($this->request->data)) {
                $this->Flash->success(__('Activation link has been sent to your email.'));

                $Email = new CakeEmail();

                $Email->emailFormat('html')
                    ->template('activation')
                    ->from(array('zteverogers@gmail.com' => 'Microblog'))
                    ->to($this->request->data['User']['email'])
                    ->subject('Microblog Activation Link')
                    ->viewVars(array('activationUrl' => 'http://' . env('SERVER_NAME') . $this->base . '/users/activate/' . $activation_key))
                    ->send();

                return $this->redirect(array('action' => 'add'));
            }

            if (!$this->User->validates()) {

                $this->set(
                    'errors',
                    $this->User->validationErrors
                );

                $this->set(
                    'data',
                    $this->request->data
                );
            }

        }
    }

    /**
     * Activate a user account.
     *
     * @param int $activation_key The key to activate the user
     *
     * @return string user error exeption
     * @throws Exception If something goes wrong.
     */
    public function activate($activation_key)
    {
        $userData = $this->User->find('first', array(
            'conditions' => array(
                'User.code_of_activation' => $activation_key,
                'User.email_activated IS NULL',
            ),
        ));

        if (!empty($userData)) {

            $this->User->id = $userData['User']['id'];
            $this->User->set(array('code_of_activation' => '-',
                'email_activated' => DboSource::expression('NOW()'),
                'image' => 'default-profile.jpg'));
            $this->User->save();

            $this->Flash->success('Your account has been activated. You may now login to access your account.');

            return $this->redirect(array('action' => 'login'));
        } else {

            $this->Flash->default('Invalid request.');
            return $this->redirect(array('action' => 'login'));

        }

    }

    /**
     * Edit a user account.
     *
     * @param int $id The user id
     *
     * @return string user error exeption
     * @throws Exception If something goes wrong.
     */
    public function edit($id = null)
    {
        if (!$id) {
            throw new NotFoundException(__('Invalid request'));
        }

        $this->loadModel('User');
        $userData = $this->User->find('first', array(
            'conditions' => array(
                'User.id' => $this->Auth->user('id'),
                'User.email_activated IS NOT NULL',
            ),
        ));

        if ($this->request->is('post')) {

            if (!empty($_FILES['postImage']['name'])) {

                if (!getimagesize($_FILES['postImage']["tmp_name"])) {
                    $this->Flash->default('Image file is supported.');
                    return $this->redirect($this->referer());
                }

                $uploadPath = WWW_ROOT . 'img' . DS;
                $imageFileType = strtolower(pathinfo($uploadPath . basename($_FILES['postImage']["name"]), PATHINFO_EXTENSION));

                if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
                    $this->Flash->set('You can only upload .jpg, .png and .jpeg files.');
                    return $this->redirect($this->referer());
                    return;
                }

                $fileName = $this->Auth->user('id') . time() . "." . $imageFileType;

                if (move_uploaded_file($_FILES['postImage']['tmp_name'], $uploadPath . $fileName)) {
                    $this->User->id = $this->Auth->user('id');
                    $this->User->set(array('image' => $fileName));
                    $this->User->save();
                }
            }

            $this->User->id = $this->Auth->user('id');
            $this->User->set(array('modified' => DboSource::expression('NOW()')));
            $this->User->save();

            $checkEmail = $this->User->find('first', array(
                'conditions' => array(
                    'User.id !=' => $this->Auth->user('id'),
                    'User.email' => $this->request->data['email'],
                ),
            ));

            $checkUsername = $this->User->find('first', array(
                'conditions' => array(
                    'User.id !=' => $this->Auth->user('id'),
                    'User.username' => $this->request->data['username'],
                ),
            ));

            if (!empty($checkEmail)) {
                SessionComponent::write('email', 'EmailExist');
                SessionComponent::write('emailContent', $this->request->data['email']);
                return $this->redirect($this->referer());
            }

            if (!empty($checkUsername)) {
                SessionComponent::write('username', 'UsernameExist');
                SessionComponent::write('usernameContent', $this->request->data['username']);
                return $this->redirect($this->referer());
            }

            if (!empty($this->request->data['currentPassword'])) {

                if (!$this->checkPassword($this->request->data['currentPassword'], $userData['User']['password'])) {
                    SessionComponent::write('password', 'Incorrect');
                    return $this->redirect($this->referer());
                } else {

                    $this->User->id = $this->Auth->user('id');
                    $this->User->set(array("password" => $this->request->data['newPassword']));
                    $this->User->save();
                }
            }

            if (!empty($this->request->data)) {
                $this->User->id = $this->Auth->user('id');
                if ($this->request->data['name'] != $userData['User']['full_name']) {
                    $this->User->set(array('full_name' => $this->request->data['name']));
                    $this->User->save();
                }
                if ($this->request->data['email'] != $userData['User']['email']) {
                    $this->User->set(array('email' => $this->request->data['email']));
                    $this->User->save();
                }
                if ($this->request->data['username'] != $userData['User']['username']) {
                    $this->User->set(array('username' => $this->request->data['username']));
                    $this->User->save();
                }
            }
        
            $this->Flash->success('Data successfully saved');
            $this->set('userData', $userData);
            $this->redirect($this->referer());
            return;
        }

        if (!$this->request->data) {
            $userData = $this->User->find('first', array(
                'conditions' => array(
                    'User.id' => $this->Auth->user('id'),
                    'User.email_activated IS NOT NULL',
                ),
            ));
            $this->set('userData', $userData);
        }

    }

    /**
     * Compare current password to new password
     *
     * @param $inputPassword The new password
     * @param $user The current password
     *
     * @return boolean true(if it is the same) false(not the same)
     */
    public function checkPassword($inputPassword, $user)
    {
        return (new BlowfishPasswordHasher)->check($inputPassword, $user);
    }

    /**
     * Function to  login user
     *
     * @return string user error exeption
     * @throws Exception If something goes wrong.
     */
    public function login()
    {
        if ($this->request->is('post')) {

            if ($this->Auth->login()) {

                $userData = $this->User->find('first', array(
                    'conditions' => array(
                        'User.id' => $this->Auth->user('id'),
                        'User.email_activated IS NULL',
                    ),
                ));

                if (!empty($userData)) {
                    $this->Flash->default('You may activated your account first. Please check your email.');
                } else {
                    return $this->redirect($this->Auth->redirectUrl());
                }

            } else {
                $this->Flash->default('Invalid username or password, try again');
            }
        }
    }

    /**
     * Logout the user and clear hisF/her session.
     *
     * @return view to login page
     */
    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    /**
     * Follow a user
     *
     * @param $id The user id
     *
     * @return string follow error exeption
     * @throws Exception If something goes wrong.
     */
    public function follow($id = null)
    {
        if ($this->request->is('post')) {

            if ($id == $this->Auth->user('id')) {
                $this->Flash->default('Invalid request. Please try again.');
                return $this->redirect($this->referer());
                return;
            }

            $this->loadModel('Follow');
            $this->Follow->create();
            $followData = $this->Follow->find('first', array(
                'conditions' => array(
                    'Follow.user_id' => $id,
                    'Follow.follower_user_id' => $this->Auth->user('id'),
                ),
            ));
            if (!empty($followData)) {
                $this->Follow->id = $followData['Follow']['id'];
                $this->Follow->set(array('is_deleted' => 0));
                $this->Follow->save();
            } else {

                $this->request->data['Follow']['user_id'] = $id;
                $this->request->data['Follow']['follower_user_id'] = $this->Auth->user('id');
                if ($this->Follow->save($this->request->data)) {
                } else {
                    $this->Flash->default('Invalid request. Please try again.');
                }
            }
            return $this->redirect($this->referer());
        }
    }

    /**
     * Unfollow a user
     *
     * @param $id The user id
     *
     * @return string unfollow error exeption
     * @throws Exception If something goes wrong.
     */
    public function unfollow($id = null)
    {
        $this->loadModel('Follow');
        if ($this->Follow->isOwnedBy($id, $this->Auth->user('id'))) {

            if ($id == $this->Auth->user('id')) {
                $this->Flash->default('Invalid request. Please try again.');
                return $this->redirect($this->referer());
                return;
            }

            if ($this->request->is('get')) {
                throw new MethodNotAllowedException();
            }
            $this->loadModel('Follow');

            $followData = $this->Follow->find('first', array(
                'conditions' => array(
                    'Follow.user_id' => $id,
                    'Follow.follower_user_id' => $this->Auth->user('id'),
                ),
            ));
            if (!empty($followData)) {
                $this->Follow->id = $followData['Follow']['id'];
                $this->Follow->set(array('is_deleted' => 1,
                    'deleted' => date('Y-m-d H:i:s')));
                $this->Follow->save();
            } else {
                $this->Flash->default('The user with id: %s could not be unfollowed.', h($id));
            }
            return $this->redirect($this->referer());
        }
    }

    /**
     * Recover an account
     * This will send an email the will change the password of the user.
     *
     * @return string forgotPassword error exeption
     * @throws Exception If something goes wrong.
     */
    public function forgotPassword()
    {
        if (!empty($this->request->data)) {
            $email = $this->request->data['User']['email'];
            $data = $this->User->find('first', array(
                'conditions' => array(
                    'User.email' => $email,
                ))
            );

            if (!empty($data)) {
                $this->request->data['User']['code_of_activation'] = $activation_key = time();
                $Email = new CakeEmail();
                $this->User->id = $data['User']['id'];
                if ($this->User->saveField('code_of_activation', $activation_key)) {
                    $Email->emailFormat('html')
                        ->template('reset_password')
                        ->from(array('zteverogers@gmail.com' => 'Microblog'))
                        ->to($this->request->data['User']['email'])
                        ->subject('Microblog Reset Password Link')
                        ->viewVars(array('activationUrl' => 'http://' . env('SERVER_NAME') . $this->base . '/users/resetPassword/' . $activation_key))
                        ->send();

                    $this->Flash->success(
                        __('Check your email to reset your password.')
                    );
                }

            } else {
                $this->Flash->default(
                    __("We couldn't find your account with that information.")
                );
            }
            foreach ($this->User->validationErrors as $error) {
                $this->Flash->default(($error[0]));
            }

        }
    }

    /**
     * Reset the password of the user
     *
     * @param $keycode The code to reset the password
     *
     * @return string resetPassword error exeption
     * @throws Exception If something goes wrong.
     */
    public function resetPassword($keycode = null)
    {
        if (!empty($keycode)) {
            $data = $this->User->find('first', array(
                'conditions' => array(
                    'code_of_activation' => $keycode,
                ))
            );
            if (!empty($data)) {
                $this->User->id = $data['User']['id'];
                if (!empty($this->request->data)) {
                    $this->request->data['User']['code_of_activation'] = '-';
                    if ($this->User->save($this->request->data)) {
                        $this->Flash->success(__('Your password has been changed successfully!'));
                        return $this->redirect(array('action' => 'login'));
                    }
                    foreach ($this->User->validationErrors as $error) {
                        $this->Flash->default(($error[0]));
                    }
                }
            } else {
                $this->Flash->default(
                    __("Invalid request. Please try again.")
                );
            }
        } else {
            return $this->redirect(array('action' => 'login'));
        }
    }

    /**
     * Get the users to follow using ajax
     *
     * @return array users, render element user_to_follow
     */
    public function userToFollow()
    {
        $this->autoRender = false;
        if ($this->request->is('ajax')) {
            $this->loadModel('Follow');
            $this->loadModel('User');
            $tofollow = $this->User->getUserToFollow($this->Auth->user('id'));
            $this->layout = null;
            $this->set('follows', $tofollow);
            $this->render('/Elements/user_to_follow');
        }

    }

}