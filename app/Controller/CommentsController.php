<?php
// app/Controller/CommentsController.php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
App::uses('CakeTime', 'Utility');
App::uses('DboSource', 'Model/DataSource');

/**
 * Handles logic that has something to do with Comments.
 *
 * @category Comments
 * @package  AppModel
 * @author   Rogie Mar <rmmahilum@gmail.com>
 * @license  Free https://bitbucket.org/rogie1201/microblog2/src/master/
 * @link     none
 */
class CommentsController extends AppController
{

    /**
     * Check if the user is authorized.
     *
     * @param int $user The user id.
     *
     * @return boolean true(If owned by the user) or false(not owned)
     */
    public function isAuthorized($user)
    {
        if (in_array($this->action, array('addComment', 'comment', 'deleteComment'))) {
            return true;
        }
        return false;
    }

    /**
     * Add comment of the post.
     *
     * @return json return the user info who commented in the post
     */
    public function addComment()
    {
        if ($this->request->is('ajax')) {
            $this->disableCache();
            $this->autoRender = false;
            $this->loadModel('Comments');
            $this->loadModel('Users');

            $this->Comment->create();
            $this->request->data['Comment']['post_id'] = $this->request->data['post_id'];
            $this->request->data['Comment']['user_id'] = $this->Auth->user('id');
            $this->request->data['Comment']['comment_text'] = $this->request->data['comment_text'];
            if ($this->Comment->save($this->request->data)) {
            } else {
                $this->Flash->default('Invalid request. Please try again.');
            }
            $comment_counts = $this->Comment->find('all', array(
                'conditions' => array(
                    'Comment.post_id =' => $this->request->data['post_id'],
                    'Comment.is_deleted !=' => 1,
                ),
            ));

            return json_encode(
                array(
                    "comment_counts" => count($comment_counts),
                    "full_name" => $this->Auth->user('full_name'),
                    "user_id" => $this->Auth->user('id'),
                    "image" => $this->Auth->user('image'),
                    "commentCreate" => date("Y-m-d H:i:s"),
                    "comment_text" => $this->request->data['comment_text'],
                    "comment_id" => $this->Comment->id,
                )
            );

        }
    }

    /**
     * Delete the comment.
     *
     * @param int $id The comment id.
     *
     * @return json Comments count and post id
     */
    public function deleteComment($id = null)
    {
        if ($this->request->is('ajax')) {
            $this->disableCache();
            $this->autoRender = false;
            $this->loadModel('Comments');

            if ($this->request->is('get')) {
                throw new MethodNotAllowedException();
            }

            $userData = $this->Comment->find('first', array(
                'conditions' => array(
                    'Comment.id' => $id,
                    'Comment.user_id' => $this->Auth->user('id'),
                ),
            ));

            if (!empty($userData)) {
                if ($id) {
                    $this->Comment->id = $id;
                    $this->Comment->set(array('is_deleted' => 1, 'deleted' => date('Y-m-d H:i:s')));
                    $this->Comment->save();
                } else {
                    $this->Flash->default('Invalid request. Please try again.');
                }
                $post = $this->Comment->findById($id);
                $comment_counts = $this->Comment->find('all', array(
                    'conditions' => array(
                        'Comment.post_id =' => $post['Comment']['post_id'],
                        'Comment.is_deleted !=' => 1,
                    ),
                ));

                return json_encode(
                    array(
                        "comment_counts" => count($comment_counts),
                        "post_id" => $post['Comment']['post_id'],
                    )
                );
            }
        }

    }

}
