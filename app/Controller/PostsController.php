<?php
// app/Controller/PostsController.php
App::uses('CakeTime', 'Utility');

/**
 * Handles logic that has something to do with Posts.
 *
 * @category Posts
 * @package  AppModel
 * @author   Rogie Mar <rmmahilum@gmail.com>
 * @license  Free https://bitbucket.org/rogie1201/microblog2/src/master/
 * @link     None
 */
class PostsController extends AppController
{
    public $helpers = array('Html', 'Form');
    public $components = array('Paginator');

    /**
     * Check if the user is authorized.
     *
     * @param int $user The user id.
     *
     * @return boolean true(If owned by the user) or false(not owned)
     */
    public function isAuthorized($user)
    {
        if (in_array($this->action, array('add', 'likes', 'comment', 'repost', 'addComment', 'index'))) {
            return true;
        }
        if (in_array($this->action, array('edit', 'delete', 'view', 'deleteImage'))) {
            $postId = (int)$this->request->params['pass'][0];
            if ($this->Post->isOwnedBy($postId, $user['id'])) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get all posts followed the user. Get number of followers and followings
     *
     * @return array list of posts, followers, followings
     */
    public function index()
    {
        $this->Paginator->settings = $this->paginate;

        //$this->clear_cache();

        $this->loadModel('Follow');
        $this->loadModel('Post');
        $this->loadModel('User');
        $this->loadModel('PostView');

        //$this->set('posts', $this->PostView->getFeedPost($this->Auth->user('id')));

        $this->paginate = array(
            'conditions' => array('PostView.GROUP_ID' => $this->Auth->user('id'),
                'PostView.IS_DELETED_SHARED !=' => 1),
            'order' => 'PostView.MODIFIED DESC',
            'limit' => 5,
        );

        $this->set('posts', $this->paginate('PostView'));

        $tofollow = $this->User->getUserToFollow($this->Auth->user('id'));
        $followers = $this->Follow->getFollowers($this->Auth->user('id'));
        $followings = $this->Follow->getFollowings($this->Auth->user('id'));

        $this->set('follows', $tofollow);
        $this->set('followers', $followers);
        $this->set('followings', $followings);

        $followers_view = $this->Follow->getFollowers($this->Auth->user('id'));
        $followings_view = $this->Follow->getFollowings($this->Auth->user('id'));

        $this->set('followers_view', $followers_view);
        $this->set('followings_view', $followings_view);

    }

    /**
     * Add a post with image(optional).
     *
     * @return void
     */
    public function add()
    {
        if ($this->request->is('post')) {
            if (strlen($this->request->data['post_txt']) == 0) {
                $this->Flash->default('Please input a description of your post.');
                $this->redirect(array('controller' => 'posts', 'action' => 'index'));
                return;
            }

            $post = array(
                "Post" => array(
                    "content" => nl2br($this->request->data['post_txt']),
                    "user_id" => $this->Auth->user('id'),
                ),
            );
            if (!$this->Post->save($post)) {
                $this->Flash->default('There has been an error while trying to save your post.');
                $this->redirect(array('controller' => 'posts', 'action' => 'index'));
                return;
            }

            if (empty($_FILES['postImage']['name'])) {
                $this->Flash->success('Content successfully posted.');
                $this->redirect(array('controller' => 'posts', 'action' => 'index'));
                return;
            }

            if (!getimagesize($_FILES['postImage']["tmp_name"])) {
                $this->Flash->default('Image file is supported.');
                $this->redirect(array('controller' => 'posts', 'action' => 'index'));
                return;
            }

            $uploadPath = WWW_ROOT . 'img' . DS;
            $imageFileType = strtolower(pathinfo($uploadPath . basename($_FILES['postImage']["name"]), PATHINFO_EXTENSION));

            if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
                $this->Flash->set('You can only upload .jpg, .png and .jpeg files.');
                $this->redirect(array('controller' => 'posts', 'action' => 'index'));
                return;
            }
            $post_id = $this->Post->id;

            $tmp = $_FILES['postImage']["tmp_name"];
            $target = WWW_ROOT . 'img' . DS;

            $image = CakeTime::convert(time(), new DateTimeZone('Asia/Manila')) .
            Security::hash($_FILES['postImage']["name"]);

            $target = $target . basename($image);

            move_uploaded_file($tmp, $target);

            $this->Post->id = $post_id;
            $this->Post->set(array('image' => $image));
            $this->Post->save();

            $this->Flash->success('Content successfully posted.');
            $this->redirect(array('controller' => 'posts', 'action' => 'index'));
        }
    }

    /**
     * View the details of the post.
     *
     * @param int $id The post id
     *
     * @return array post, comments, followers, followings
     */
    public function view($id = null)
    {
        $this->loadModel('PostView');
        $this->loadModel('Comment');
        $this->loadModel('User');
        if (!$id) {
            $this->render('not_found');
            return;
        }

        $post = $this->PostView->find('first', array(
            'conditions' => array(
                'PostView.POST_ID =' => $id,
                'PostView.GROUP_ID =' => $this->Auth->user('id'),
            ),
        ));

        $this->paginate = array(
            "conditions" => array(
                "Comment.is_deleted" => 0,
                "User.is_deleted" => 0,
                "Comment.post_id" => $id,
            ),
            "joins" => array(
                array(
                    'table' => 'users',
                    'alias' => 'User',
                    'type' => 'INNER',
                    'conditions' => array(
                        'Comment.user_id = User.id',
                    ),
                ),
            ),
            "fields" => array(
                "User.full_name",
                "User.username",
                "User.id",
                "User.image",
                "Comment.modified",
                "Comment.comment_text",
                "Comment.id",
                "Comment.post_id",
            ),
            'limit' => 5,
            'order' => 'Comment.modified DESC',
        );

        if (!$post) {
            $this->render('not_found');
            return;
        }

        $this->set('posts', $post);
        $this->set('comments', $this->paginate('Comment'));

        $this->loadModel('Follow');

        $tofollow = $this->User->getUserToFollow($this->Auth->user('id'));
        $followers_view = $this->Follow->getFollowers($this->Auth->user('id'));
        $followings_view = $this->Follow->getFollowings($this->Auth->user('id'));

        $this->set('followers_view', $followers_view);
        $this->set('followings_view', $followings_view);
        $this->set('follows', $tofollow);

    }

    /**
     * Edit the post.
     *
     * @param int $id The post id
     *
     * @return string posts error exeption
     * @throws Exception If something goes wrong.
     */
    public function edit($id = null)
    {
        if (!$id) {
            throw new NotFoundException(__('Invalid post'));
        }

        $post = $this->Post->findById($id);
        if (!$post) {
            throw new NotFoundException(__('Invalid post'));
        }

        if ($this->Post->isOwnedBy($id, $this->Auth->user('id'))) {

            if ($this->request->is('post')) {

                if (strlen($this->request->data['post_txt']) == 0) {
                    $this->Flash->default('Please input a description of your post.');
                    return $this->redirect($this->referer());
                }

                $post = array(
                    "Post" => array(
                        "content" => nl2br($this->request->data['post_txt']),
                        "user_id" => $this->Auth->user('id'),
                    ),
                );

                $this->Post->id = $id; //$this->request->data['post_id'];
                if (!$this->Post->save($post)) {
                    $this->Flash->default('There has been an error while trying to save your post.');
                    $this->redirect(array('controller' => 'posts', 'action' => 'index'));
                    return;
                }

                if (empty($_FILES['postImage']['name'])) {

                    $this->Post->id = $id;
                    $this->Flash->success('Content successfully posted.');
                    $this->redirect(array('controller' => 'posts', 'action' => 'index'));
                    return;
                }

                if (!getimagesize($_FILES['postImage']["tmp_name"])) {
                    $this->Flash->default('Image file is supported.');
                    $this->redirect(array('controller' => 'posts', 'action' => 'index'));
                    return;
                }

                $uploadPath = WWW_ROOT . 'img' . DS;

                $imageFileType = strtolower(pathinfo($uploadPath . basename($_FILES['postImage']["name"]), PATHINFO_EXTENSION));

                if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
                    $this->Flash->set('You can only upload .jpg, .png and .jpeg files.');
                    $this->redirect(array('controller' => 'posts', 'action' => 'index'));
                    return;
                }

                $fileName = $this->request->data['post_id'] . time() . "." . $imageFileType;
                if (move_uploaded_file($_FILES['postImage']['tmp_name'], $uploadPath . $fileName)) {
                    $this->Post->id = $id;
                    $this->Post->set(array('image' => $fileName));
                    $this->Post->save();
                } else {
                    $this->Flash->default('Something went wrong while trying to upload the image.');
                    $this->redirect(array('controller' => 'posts', 'action' => 'index'));
                }
                $this->Flash->success('Content successfully posted.');
                $this->redirect(array('controller' => 'posts', 'action' => 'index'));
            }
        } else {
            $this->Flash->default('Invalid request. Please try again.');
            return $this->redirect($this->referer());
            return;
        }

        if (!$this->request->data) {
            $this->set('posts', $post);
        }
    }

    /**
     * Delete the post.
     *
     * @param int $id The post id
     *
     * @return string posts error exeption
     * @throws Exception If something goes wrong.
     */
    public function delete($id = null)
    {
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }

        if ($id) {
            $this->Post->id = $id;
            $this->Post->set(array('is_deleted' => 1, 'deleted' => date('Y-m-d H:i:s')));
            $this->Post->save();
        } else {
            $this->Flash->default(__('The post with id: %s could not be deleted.', h($id)));
        }
        $this->redirect(array('controller' => 'posts', 'action' => 'index'));
    }

    /**
     * Delete the image post.
     *
     * @param int $id The post id
     *
     * @return string posts error exeption
     * @throws Exception If something goes wrong.
     */
    public function deleteImage($id = null)
    {
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }
        if ($id) {
            $post_data = $this->Post->findById($id);
            $this->request->data = $post_data;
            $directory = WWW_ROOT . 'img' . DS;

            if (file_exists($directory . DIRECTORY_SEPARATOR . $post_data['Post']['image'])) {
                if (unlink($directory . DIRECTORY_SEPARATOR . $post_data['Post']['image'])) {

                    $this->Flash->success(__('Image successfully removed.'));
                    $this->Post->id = $id;
                    $this->Post->set(array('image' => null));
                    $this->Post->save();

                    $this->redirect(array('controller' => 'posts', 'action' => 'edit/' . $id));
                }
            }
        } else {
            $this->Flash->default(__('Cannot be deleted.'));
        }
    }

    /**
     * Share the post.
     *
     * @param int $id The post id
     *
     * @return string posts error exeption
     * @throws Exception If something goes wrong.
     */
    public function repost($id = null)
    {
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }

        $this->loadModel('PostView');
        $get_content = $this->PostView->find('first', array(
            'conditions' => array('PostView.POST_ID' => $id),
        ));

        if (!$get_content) {
            throw new NotFoundException(__('Invalid post'));
        }
        if ($id) {

            $post = array(
                "Post" => array(
                    "repost_id" => $id,
                    "user_id" => $this->Auth->user('id'),
                    "content" => nl2br($get_content['PostView']['CONTENT']),
                    "image" => nl2br($get_content['PostView']['IMAGE']),
                ));
            if ($this->Post->save($post)) {
                $this->Flash->success('Successfully shared.');
                $this->redirect(array('controller' => 'posts', 'action' => 'index'));
            }
        }
        $this->redirect(array('controller' => 'posts', 'action' => 'index'));
    }

    /**
     * Get comments of the post limit by 3.
     *
     * @param int $id The post id
     *
     * @return array comments, int post_id, render element comment
     */
    public function comment($id = null)
    {

        $this->autoRender = false;
        if (!$id) {
            throw new NotFoundException(__('Invalid post'));
        }

        if ($this->request->is('ajax')) {
            $this->loadModel('Comment');

            $this->paginate = array(
                "conditions" => array(
                    "Comment.is_deleted" => 0,
                    "User.is_deleted" => 0,
                    "Comment.post_id" => $id,
                ),
                "joins" => array(
                    array(
                        'table' => 'users',
                        'alias' => 'User',
                        'type' => 'INNER',
                        'conditions' => array(
                            'Comment.user_id = User.id',
                        ),
                    ),
                ),
                "fields" => array(
                    "User.full_name",
                    "User.username",
                    "User.id",
                    "User.image",
                    "Comment.modified",
                    "Comment.comment_text",
                    "Comment.id",
                    "Comment.post_id",
                ),
                'limit' => 3,
                'order' => 'Comment.modified DESC',
            );

            $this->layout = null;
            $this->set('comments', $this->paginate('Comment'));
            $this->set('post_id', $id);
            $this->render('/Elements/comment');

        }
    }

}
