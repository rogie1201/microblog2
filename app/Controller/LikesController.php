<?php
// app/Controller/LikesController.php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
App::uses('CakeTime', 'Utility');
App::uses('DboSource', 'Model/DataSource');
/**
 * Handles logic that has something to do with Likes.
 *
 * @category Likes
 * @package  AppModel
 * @author   Rogie Mar <rmmahilum@gmail.com>
 * @license  Free https://bitbucket.org/rogie1201/microblog2/src/master/
 * @link     None
 */
class LikesController extends AppController
{

    /**
     * Check if the user is authorized.
     *
     * @param int $user The user id.
     *
     * @return boolean true(If owned by the user) or false(not owned)
     */
    public function isAuthorized($user)
    {
        if (in_array($this->action, array('like', 'unlike'))) {
            return true;
        }
        return false;
    }

    /**
     * Like a post.
     *
     * @return int count of likes
     */
    public function like()
    {
        if ($this->request->is('ajax')) {
            $this->disableCache();
            $this->autoRender = false;
            $this->loadModel('PostView');

            $this->Like->create();
            $likeData = $this->Like->find('first', array(
                'conditions' => array(
                    'Like.post_id' => $this->request->data['post_id'],
                    'Like.user_id' => $this->Auth->user('id'),
                ),
            ));
            if (!empty($likeData)) {
                $this->Like->id = $likeData['Like']['id'];
                $this->Like->set(array('is_deleted' => 0));
                $this->Like->save();
            } else {
                $this->request->data['Like']['post_id'] = $this->request->data['post_id'];
                $this->request->data['Like']['user_id'] = $this->Auth->user('id');
                if ($this->Like->save($this->request->data)) {
                } else {
                    $this->Flash->default('Invalid request. Please try again.');
                }
            }

            $like_counts = $this->PostView->find('first', array(
                'conditions' => array(
                    'PostView.POST_ID =' => $this->request->data['post_id'],
                    'PostView.GROUP_ID =' => $this->Auth->user('id'),
                ),
            ));
            return $like_counts['PostView']['LIKES_COUNT'];
        }
    }

    /**
     * Unlike a post.
     *
     * @return int count of likes
     */
    public function unlike()
    {
        if ($this->request->is('ajax')) {
            $this->disableCache();
            $this->autoRender = false;
            $this->loadModel('PostView');

            $likeData = $this->Like->find('first', array(
                'conditions' => array(
                    'Like.post_id' => $this->request->data['post_id'],
                    'Like.user_id' => $this->Auth->user('id'),
                ),
            ));
            if (!empty($likeData)) {
                $this->Like->id = $likeData['Like']['id'];
                $this->Like->set(array('is_deleted' => 1,
                    'deleted' => date('Y-m-d H:i:s')));
                $this->Like->save();
            } else {
                $this->Flash->default('Invalid request. Please try again.');
            }

            $like_counts = $this->PostView->find('first', array(
                'conditions' => array(
                    'PostView.POST_ID =' => $this->request->data['post_id'],
                    'PostView.GROUP_ID =' => $this->Auth->user('id'),
                ),
            ));
            return $like_counts['PostView']['LIKES_COUNT'];
        }
    }

}
