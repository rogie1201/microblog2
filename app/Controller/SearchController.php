<?php

// app/Controller/SearchController.php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
App::uses('CakeTime', 'Utility');

/**
 * Handles logic that has something to do with Search.
 *
 * @category Search
 * @package  AppModel
 * @author   Rogie Mar <rmmahilum@gmail.com>
 * @license  Free https://bitbucket.org/rogie1201/microblog2/src/master/
 * @link     None
 */
class SearchController extends AppController
{
    public $helpers = array('Html', 'Form');
    public $components = array('Paginator');

    /**
     * Check if the user is authorized.
     *
     * @param int $user The user id.
     *
     * @return boolean true(If owned by the user) or false(not owned)
     */
    public function isAuthorized($user)
    {
        if (in_array($this->action, array('searchPosts', 'searchPeople'))) {
            return true;
        }
        return false;
    }

    /**
     * Search posts.
     *
     * @param string $keyword The keyword to search
     *
     * @return array posts, followers, followings
     */
    public function searchPosts($keyword = null)
    {
        $this->Paginator->settings = $this->paginate;

        $this->loadModel('Follow');
        $this->loadModel('Post');
        $this->loadModel('User');
        $this->loadModel('PostView');

        $followers = $this->Follow->getFollowers($this->Auth->user('id'));
        $followings = $this->Follow->getFollowings($this->Auth->user('id'));
        $tofollow = $this->User->getUserToFollow($this->Auth->user('id'));

        $this->set('followers', $followers);
        $this->set('followings', $followings);
        $this->set('follows', $tofollow);

        $followers_view = $this->Follow->getFollowers($this->Auth->user('id'));
        $followings_view = $this->Follow->getFollowings($this->Auth->user('id'));

        $this->set('followers_view', $followers_view);
        $this->set('followings_view', $followings_view);

        $this->set('posts', 0);
        $this->set('people', 0);

        if (!empty($this->request->query['keyword'])) {
            $keyword = $this->request->query['keyword'];
        } else {
            $keyword = $keyword;
        }

        if (!empty($keyword)) {

            //Query For Posts
            $this->paginate = array(
                'conditions' => array('PostView.content LIKE' => "%" . trim($keyword) . "%",
                    'PostView.GROUP_ID' => $this->Auth->user('id')),
                'order' => 'PostView.MODIFIED DESC',
                'limit' => 3,
            );

            $this->set('posts', $this->paginate('PostView'));
            $this->set('keyword_search', $keyword);

        }
    }

    /**
     * Search people.
     *
     * @param string $keyword The keyword to search
     *
     * @return array people, followers, followings
     */
    public function searchPeople($keyword = null)
    {
        $this->Paginator->settings = $this->paginate;

        $this->loadModel('Follow');
        $this->loadModel('Post');
        $this->loadModel('User');

        $followers = $this->Follow->getFollowers($this->Auth->user('id'));
        $followings = $this->Follow->getFollowings($this->Auth->user('id'));
        $tofollow = $this->User->getUserToFollow($this->Auth->user('id'));

        $this->set('followers', $followers);
        $this->set('followings', $followings);
        $this->set('follows', $tofollow);

        $followers_view = $this->Follow->getFollowers($this->Auth->user('id'));
        $followings_view = $this->Follow->getFollowings($this->Auth->user('id'));

        $this->set('followers_view', $followers_view);
        $this->set('followings_view', $followings_view);

        $this->set('posts', 0);
        $this->set('people', 0);

        if (!empty($this->request->query['keyword'])) {
            $keyword = $this->request->query['keyword'];
        } else {
            $keyword = $keyword;
        }

        if (!empty($keyword)) {

            //Query for People
            $this->set('people', $this->User->getPeople($this->Auth->user('id')));

            $cond = array();
            $cond['User.full_name LIKE'] = "%" . trim($keyword) . "%";
            $cond['User.username LIKE'] = "%" . trim($keyword) . "%";
            $cond['User.email LIKE'] = "%" . trim($keyword) . "%";
            $conditions['OR'] = $cond;

            $this->paginate = array(
                'conditions' => $conditions,
                'order' => 'User.MODIFIED DESC',
                'limit' => 3,
            );

            $this->set('people', $this->paginate('User'));
            $this->set('keyword_search', $keyword);
        }
    }

}