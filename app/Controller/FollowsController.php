<?php
// app/Controller/FollowsController.php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
App::uses('CakeTime', 'Utility');
/**
 * Handles logic that has something to do with Follows.
 *
 * @category Follows
 * @package  AppModel
 * @author   Rogie Mar <rmmahilum@gmail.com>
 * @license  Free https://bitbucket.org/rogie1201/microblog2/src/master/
 * @link     none
 */
class FollowsController extends AppController
{
    public $helpers = array('Html', 'Form');
    public $components = array('Paginator');

    /**
     * Check if the user is authorized.
     *
     * @param int $user The user id.
     *
     * @return boolean true(If owned by the user) or false(not owned)
     */
    public function isAuthorized($user)
    {
        if (in_array($this->action, array('followers', 'following'))) {
            return true;
        }
        return false;
    }

    /**
     * Get all the followers.
     *
     * @param int $follow_type 1 - Followers, 2 - Following.
     * @param int $id The user id
     *
     * @return array list of user
     */
    public function followers($follow_type = null, $id = null)
    {
        $this->Paginator->settings = $this->paginate;

        $this->loadModel('Follow');
        $this->loadModel('Post');
        $this->loadModel('User');

        $followers = $this->Follow->getFollowers($id);
        $followings = $this->Follow->getFollowings($id);
        $tofollow = $this->User->getUserToFollow($this->Auth->user('id'));

        $followers_view = $this->Follow->getFollowers($this->Auth->user('id'));
        $followings_view = $this->Follow->getFollowings($this->Auth->user('id'));

        $this->paginate =
        array(
            "conditions" => array(
                "Follow.user_id" => $id,
                "Follow.is_deleted" => 0,
                "User.is_deleted" => 0,
                "User.id !=" => $id,
            ),
            "joins" => array(
                array(
                    'table' => 'users',
                    'alias' => 'User',
                    'type' => 'INNER',
                    'conditions' => array(
                        'Follow.follower_user_id = User.id',
                    ),
                ),
            ),
            "fields" => array(
                "User.full_name",
                "User.username",
                "User.id",
                "User.image",
                "Follow.id",
                "Follow.created",
                "Follow.user_id",
                "Follow.follower_user_id",
            ),
            'limit' => 2,
        );

        $this->set('followers', $this->paginate('Follow'));
        $this->set('followings', $followings);
        $this->set('follows', $tofollow);
        $this->set('follow_type', $follow_type);

        $getPeople = $this->User->getPeople($this->Auth->user('id'));

        $this->set('followers_view', $followers_view);
        $this->set('followings_view', $followings_view);

        $this->set('getPeople', $getPeople);
        $this->set('user_id', $id);

        $user = $this->User->find('first', array(
            'conditions' => array(
                'User.id =' => $id,
            ),
        ));

        $this->set('user_selected', $user);
    }

    /**
     * Get all the followings.
     *
     * @param int $follow_type 1 - Followers, 2 - Following.
     * @param int $id The user id
     *
     * @return array list of user
     */
    public function following($follow_type = null, $id = null)
    {
        $this->Paginator->settings = $this->paginate;

        $this->loadModel('Follow');
        $this->loadModel('Post');
        $this->loadModel('User');

        $followers = $this->Follow->getFollowers($id);
        $followings = $this->Follow->getFollowings($id);
        $tofollow = $this->User->getUserToFollow($this->Auth->user('id'));

        $this->paginate = array(
            "conditions" => array(
                "Follow.follower_user_id" => $id,
                "Follow.is_deleted" => 0,
                "User.is_deleted" => 0,
                "Follow.user_id !=" => $id,
            ),
            "joins" => array(
                array(
                    'table' => 'users',
                    'alias' => 'User',
                    'type' => 'INNER',
                    'conditions' => array(
                        'Follow.user_id = User.id',
                    ),
                ),
            ),
            "fields" => array(
                "User.full_name",
                "User.username",
                "User.id",
                "User.image",
                "Follow.created",
                "Follow.id",
            ),
            'limit' => 2,
        );

        $this->set('followers', $followers);
        $this->set('followings', $this->paginate('Follow'));
        $this->set('follows', $tofollow);
        $this->set('follow_type', $follow_type);

        $followers_view = $this->Follow->getFollowers($this->Auth->user('id'));
        $followings_view = $this->Follow->getFollowings($this->Auth->user('id'));

        $getPeople = $this->User->getPeople($this->Auth->user('id'));

        $this->set('followers_view', $followers_view);
        $this->set('followings_view', $followings_view);

        $this->set('getPeople', $getPeople);
        $this->set('user_id', $id);

        $user = $this->User->find('first', array(
            'conditions' => array(
                'User.id =' => $id,
            ),
        ));

        $this->set('user_selected', $user);
    }

}
