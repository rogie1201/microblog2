<?php
// app/Model/Comment.php
App::uses('AppModel', 'Model');
/**
 * Handles logic that has something to do with Comments.
 *
 * @category Comment
 * @package  AppModel
 * @author   Rogie Mar <rmmahilum@gmail.com>
 * @license  Free https://bitbucket.org/rogie1201/microblog2/src/master/
 * @link     none
 */
class Comment extends AppModel
{

    /**
     * Check if it is owned by the user.
     *
     * @param int $comment comment id.
     * @param int $user user id.
     *
     * @return boolean true(If owned by the user) or false(not owned)
     */
    public function isOwnedBy($comment, $user)
    {
        return $this->field('id', array('id' => $comment, 'user_id' => $user)) !== false;
    }

}