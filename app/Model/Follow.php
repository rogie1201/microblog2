<?php
// app/Model/Follow.php
App::uses('AppModel', 'Model');
/**
 * Handles logic that has something to do with Follow.
 *
 * @category Follow
 * @package  AppModel
 * @author   Rogie Mar <rmmahilum@gmail.com>
 * @license  Free https://bitbucket.org/rogie1201/microblog2/src/master/
 * @link     none
 */
class Follow extends AppModel
{

    /**
     * Query all the users who is being followed by this user.
     *
     * @param int $user_id The user id.
     *
     * @return array An array of users.
     */
    public function getFollowings($user_id)
    {
        return $this->find('all', array(
            "conditions" => array(
                "Follow.follower_user_id" => $user_id,
                "Follow.is_deleted" => 0,
                "User.is_deleted" => 0,
                "Follow.user_id !=" => $user_id,
            ),
            "joins" => array(
                array(
                    'table' => 'users',
                    'alias' => 'User',
                    'type' => 'INNER',
                    'conditions' => array(
                        'Follow.user_id = User.id',
                    ),
                ),
            ),
            "fields" => array(
                "User.full_name",
                "User.username",
                "User.id",
                "User.image",
                "Follow.created",
                "Follow.id",
            ),
        ));
    }

    /**
     * Query all the users who is following this user.
     *
     * @param int $user_id The user id.
     *
     * @return array An array of users.
     */
    public function getFollowers($user_id)
    {
        return $this->find('all', array(
            "conditions" => array(
                "Follow.user_id" => $user_id,
                "Follow.is_deleted" => 0,
                "User.is_deleted" => 0,
                "User.id !=" => $user_id,
            ),
            "joins" => array(
                array(
                    'table' => 'users',
                    'alias' => 'User',
                    'type' => 'INNER',
                    'conditions' => array(
                        'Follow.follower_user_id = User.id',
                    ),
                ),
            ),
            "fields" => array(
                "User.full_name",
                "User.username",
                "User.id",
                "User.image",
                "Follow.id",
                "Follow.created",
                "Follow.user_id",
            ),
        ));
    }

    /**
     * Check if it is owned by the user.
     *
     * @param int $user The user id.
     * @param int $user_follower The user id of the follower.
     *
     * @return boolean true(If owned by the user) or false(not owned)
     */
    public function isOwnedBy($user, $user_follower)
    {
        return $this->field('id', array('user_id' => $user, 'follower_user_id' => $user_follower)) !== false;
    }

}