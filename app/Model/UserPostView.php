<?php
// app/Model/UserPostView.php
/**
 * Handles logic that has something to do with post.
 *
 * @package app.Model
 */
App::uses('AppModel', 'Model');
/**
 * Handles logic that has something to do with Posts.
 *
 * @category Posts
 * @package  AppModel
 * @author   Rogie Mar <rmmahilum@gmail.com>
 * @license  Free https://bitbucket.org/rogie1201/microblog2/src/master/
 * @link     Posts
 */
class UserPostView extends AppModel
{

    /**
     * Database view that query all the posts that user posted.
     *
     * @param int $user_id The user id.
     *
     * @return array An array of posts.
     */
    public function getFeedPostPerUser($user_id)
    {
        return $this->find('all', array(
            'conditions' => array('UserPostView.GROUP_ID' => $user_id),
        ));
    }

}