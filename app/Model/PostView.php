<?php
// app/Model/PostView.php
App::uses('AppModel', 'Model');
/**
 * Handles logic that has something to do with Posts.
 *
 * @category Posts
 * @package  AppModel
 * @author   Rogie Mar <rmmahilum@gmail.com>
 * @license  Free https://bitbucket.org/rogie1201/microblog2/src/master/
 * @link     none
 */
class PostView extends AppModel
{

    /**
     * Database view that query all the posts of the user followed and his/her posts.
     *
     * @param int $user_id The user id.
     * 
     * @return array An array of users.
     */
    public function getFeedPost($user_id)
    {
        return $this->find('all', array(
            'conditions' => array('PostView.GROUP_ID' => $user_id),
        ));
    }

}