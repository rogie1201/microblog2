<?php
App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');
/**
 * Handles logic that has something to do with Users.
 *
 * @category User
 * @package  AppModel
 * @author   Rogie Mar <rmmahilum@gmail.com>
 * @license  Free https://bitbucket.org/rogie1201/microblog2/src/master/
 * @link     none
 */
class User extends AppModel
{
    public $validate = array(

        'full_name' => array(
            'required' => array(
                'rule' => '/[一-龠]+|[ぁ-ゔ]+|[ァ-ヴー]+|[a-zA-Z0-9]+|[ａ-ｚＡ-Ｚ０-９]+|[々〆〤]+/u',
                'message' => 'Only letters are allowed on names and cannot be empty.',
                'allowEmpty' => false),
        ),

        'email' => array(
            'required' => array(
                'rule' => 'email',
                'message' => 'Please enter a valid email address.',
                'allowEmpty' => false,
            ),
            'checkEmail' => array(
                'rule' => 'isEmailAvailable',
                'message' => 'Email is not available.',
            ),
        ),

        'birthdate' => array(
            'required' => array(
                'rule' => 'date',
                'message' => 'Please enter a valid date.',
                'allowEmpty' => false,
            ),
            'DOB' => array(
                'rule' => 'checkDOB',
                'message' => 'Please enter a valid date.',
            ),
        ),

        'username' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'A username is required',
            ),
            'checkUsername' => array(
                'rule' => 'isUsernameAvailable',
                'message' => 'Username is not available.',
            ),
        ),
        'password' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'A password is required',
            ),
            'size' => array(
                'rule' => array('minLength', 8),
                'message' => 'Password must have a minimum of 8 characters.',
            ),
        ),

        'code_of_activation' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Verification Code is required.',
            ),
        ),

    );

    /**
     * Function that will hashed the user password using BlowfishPasswordHasher().
     *
     * @param array $options array of data.
     *
     * @return string A hashed user password.
     */
    public function beforeSave($options = array())
    {
        if (isset($this->data[$this->alias]['password'])) {
            $passwordHasher = new BlowfishPasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash(
                $this->data[$this->alias]['password']
            );
        }
        return true;
    }

    /**
     * Method that will check if email is available.
     *
     * @return boolean true(If email is available) or false(email unavailable)
     */
    public function isEmailAvailable()
    {
        $email = $this->find(
            'first',
            array(
                'conditions' => array(
                    'User.email' => $this->data[$this->alias]['email'],
                    //'User.email_activated IS NULL',
                ),
            )
        );
        if (empty($email)) {
            return true;
        }
        return false;
    }

    /**
     * Method that will get all the users that is not followed by the current user.
     *
     * @param int $user_id The user id.
     *
     * @return array List of users.
     */
    public function getUserToFollow($user_id)
    {

        $followings = $this->find('all',
            array(
                'conditions' => array(
                    'follow.follower_user_id =' => $user_id,
                    'follow.is_deleted' => 0,
                ),
                'joins' => array(
                    array(
                        'table' => 'follows',
                        'alias' => 'follow',
                        'type' => 'INNER',
                        'conditions' => array(
                            'follow.follower_user_id = User.id',
                        ),
                    ),
                ),
                "fields" => array(
                    "follow.user_id",
                ),

            )
        );

        $arrayFollowings = array();
        for ($i = 0; $i < count($followings); $i++) {
            array_push($arrayFollowings, $followings[$i]['follow']['user_id']);
        }
        //$List = implode(', ', $arrayFollowings);
        return $this->find(
            'all',
            array(
                'conditions' => array(
                    'User.id !=' => $user_id,
                    'User.is_deleted' => 0,
                    'NOT' => array(
                        'User.id' => ($arrayFollowings),
                    ),
                ),
                'order' => 'User.created DESC',
                'limit' => 5,
            )
        );
    }

    /**
     * Method that will get all the users.
     *
     * @param int $user_id The user id.
     *
     * @return array List of users.
     */
    public function getPeople($user_id)
    {
        return $this->find('all', array(
            "conditions" => array(
                "User.is_deleted" => 0,
                "User.id !=" => $user_id,
            ),
            "fields" => array(
                "User.full_name",
                "User.username",
                "User.id",
                "User.image",
            ),

        ));
    }

    /**
     * Method that will check if birth date is valid.
     *
     * @return boolean true(If it is available) or false(It is unavailable)
     */
    public function checkDOB()
    {
        if ($this->data[$this->alias]['birthdate'] == date('Y-m-d') ||
            $this->data[$this->alias]['birthdate'] > date('Y-m-d')) {
            return false;
        }
        return true;
    }

    /**
     * Check if it is owned by the user.
     *
     * @param int $user The user id.
     *
     * @return boolean true(If owned by the user) or false(not owned)
     */
    public function isOwnedBy($user)
    {
        return $this->field('id', array('id' => $user)) !== false;
    }

    /**
     * Method that will check if username is available.
     *
     * @return boolean true(If it is available) or false(It is unavailable)
     */
    public function isUsernameAvailable()
    {
        $username = $this->find(
            'first',
            array(
                'conditions' => array(
                    'User.username' => $this->data[$this->alias]['username'],
                    //'User.email_activated IS NULL',
                ),
            )
        );
        if (empty($username)) {
            return true;
        }
        return false;
    }

}
