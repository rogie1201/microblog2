<?php
// app/Model/Post.php
App::uses('AppModel', 'Model');
/**
 * Handles logic that has something to do with Posts.
 *
 * @category Post
 * @package  AppModel
 * @author   Rogie Mar <rmmahilum@gmail.com>
 * @license  Free https://bitbucket.org/rogie1201/microblog2/src/master/
 * @link     none
 */
class Post extends AppModel
{

    public $validate = array(

        'content' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Content cannot be empty.',
                'allowEmpty' => false),
            'maximumLength' => array(
                'rule' => array('maxLength', 140),
                'message' => 'You have exceeded the allowed maximum characters of 140.',
            ),
        ),

    );

    /**
     * Check if it is owned by the user.
     *
     * @param int $post The post id.
     * @param int $user The user id.
     *
     * @return boolean true(If owned by the user) or false(not owned)
     */
    public function isOwnedBy($post, $user)
    {
        return $this->field('id', array('id' => $post, 'user_id' => $user)) !== false;
    }

    /**
     * Check if it is owned by the user.
     *
     * @param int $params The tempory file.
     *
     * @return boolean true(If not empty) or false(empty file)
     */
    public function isUploadedFile($params)
    {
        if (!empty($params['tmp_name']) && $params['tmp_name'] != 'none') {
            return true;
        }
        return false;
    }
}