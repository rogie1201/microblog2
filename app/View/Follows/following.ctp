<body>

    <?=$this->element('leftnav')?>
    <?=$this->element('rightnav')?>

    <div class="container">
        <div class="container mt-4 mb-5">
            <div class="d-flex justify-content-center row">
                <div class="col-md-11">

                    <?php
if ($follow_type == 1) {
    $follower_tab = 'true';
    $follower_area = ' active';
    $follower_content = ' active show';
} else {
    $follower_tab = 'false';
    $follower_area = '';
    $follower_content = '';
}

if ($follow_type == 2) {
    $following_tab = 'true';
    $following_area = ' active';
    $following_content = ' active show';
} else {
    $following_tab = 'false';
    $following_area = '';
    $following_content = '';
}
?>

                    <button class="btn btn-lg" onclick="window.history.back()">
                        <i class="fas fa-chevron-circle-left post_liked"></i>
                    </button>

                    <div class="media">
                        <img src="<?php echo $this->webroot . "img/" . $user_selected['User']['image']; ?>" width="56"
                            height="56" class="rounded-circle mr-2">
                        <div class="media-body">

                            <?php
                                        if ($user_selected['User']['id'] != $this->Session->read('Auth.User.id')) {
                                            echo $this->Html->link($user_selected['User']['full_name'], array('controller' => 'users', 'action' => 'view', $user_selected['User']['id']), array('class' => 'font-weight-bold'));
                                        } else {
                                            echo $this->Html->link($user_selected['User']['full_name'], array('controller' => 'users', 'action' => 'index'), array('class' => 'font-weight-bold'));

                                        }
                                        ?>
                            <br>
                            <i><?php echo "@" . $user_selected['User']['username']; ?></i>

                        </div>
                    </div>

                    <br>
                    <br>

                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">


                            <?=$this->Form->postLink(
    'Followers', array(
        'controller' => 'follows',
        'action' => 'followers', 1, $user_id,
    ), array(
        'class' => 'btn btn-outline-default',
    ))?>


                            <a class="nav-item nav-link <?php echo $following_area; ?>" id="nav-profile-tab"
                                data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile"
                                aria-selected="<?php echo $following_tab; ?>">Following</a>
                        </div>
                    </nav>
                    <div class="tab-content" id="nav-tabContent">

                        <div class="tab-pane fade <?php echo $following_content; ?>" id="nav-profile" role="tabpanel"
                            aria-labelledby="nav-profile-tab">

                            <div class="card-body">
                                <?php foreach ($followings as $tofollow): ?>

                                <div class="media">
                                    <img src="<?php echo $this->webroot . "img/" . $tofollow['User']['image']; ?>"
                                        width="56" height="56" class="rounded-circle mr-2">
                                    <div class="media-body">

                                        <?php
if ($tofollow['User']['id'] != $this->Session->read('Auth.User.id')) {
    echo $this->Html->link($tofollow['User']['full_name'], array('controller' => 'users', 'action' => 'view', $tofollow['User']['id']), array('class' => 'font-weight-bold'));
} else {
    echo $this->Html->link($tofollow['User']['full_name'], array('controller' => 'users', 'action' => 'index'), array('class' => 'font-weight-bold'));

}
?>
                                        <br>


                                        <i><?php echo "@" . $tofollow['User']['username']; ?></i>


                                        <?php 
                                        if ($tofollow['User']['id'] != $this->Session->read('Auth.User.id')) {
                                        echo $this->Form->postLink('Unfollow', array('controller' => 'users', 'action' => 'unfollow', $tofollow['User']['id']),
                                                array('class' => 'btn btn-sm btn-outline-primary float-right')
                                            );
                                        }
                                        ?>

                                    </div>
                                </div>
                                <hr class="my-2">
                                <?php endforeach;?>
                                <?php unset($tofollow);?>

                            </div>

                            <?php

$paginator = $this->Paginator;

echo "<center><div class='paging'>";

echo $paginator->first('First');
echo " ";

if ($paginator->hasPrev()) {
    echo $paginator->prev('<<');
}
echo " ";
echo $paginator->numbers(array('modulus' => 2));
echo " ";

if ($paginator->hasNext()) {
    echo $paginator->next('>>');
}

echo " ";
echo $paginator->last('Last');
echo "</div></center>";

?>

                        </div>

                    </div>




                </div>
            </div>
        </div>
    </div>

    <?=$this->element('footer')?>

    </div>





</body>