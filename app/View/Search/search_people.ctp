<body>

    <?=$this->element('leftnav') ?>
    <?=$this->element('rightnav') ?>

    <div class="container">
        <div class="container mt-4 mb-5">
            <div class="d-flex justify-content-center row">
                <div class="col-md-11">

                    <h3>Search Results for: <?= (isset($keyword_search)) ? h($keyword_search) : '' ?></h3>

                    <?php

$follow_type = 2;

if ($follow_type == 1) {
    $follower_tab = 'true';
    $follower_area = ' active';
    $follower_content = ' active show';
} else {
    $follower_tab = 'false';
    $follower_area = '';
    $follower_content = '';
}

if ($follow_type == 2) {
    $following_tab = 'true';
    $following_area = ' active';
    $following_content = ' active show';
} else {
    $following_tab = 'false';
    $following_area = '';
    $following_content = '';
}
?>

                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">

                            <?=$this->Form->postLink(
    'Posts', array(
        'controller' => 'search',
        'action' => 'searchPosts',
        (isset($keyword_search)) ? stripslashes(preg_replace('#/+#', '', $keyword_search))  : '',
    ), array(
        'class' => 'btn btn-outline-default',
    )) ?>


                            <a class="nav-item nav-link <?php echo $following_area; ?>" id="nav-profile-tab"
                                data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile"
                                aria-selected="<?php echo $following_tab; ?>">People</a>


                        </div>
                    </nav>

                    <div class="tab-pane fade <?php echo $following_content; ?>" id="nav-profile" role="tabpanel"
                        aria-labelledby="nav-profile-tab">

                        <div class="card-body">
                            <?php

if ($people != 0) {

    if (count($people) == 0) {

        echo '<div class="text-muted"><small>NO MATCHES FOUND <br> Please try another search.</small></div>';

    }

    foreach ($people as $peoples): ?>

                            <div class="media">
                                <img src="<?php echo $this->webroot . "img/" . $peoples['User']['image']; ?>" width="56"
                                    height="56" class="rounded-circle mr-2">


                                <div class="media-body">
                                    <!-- <p class="my-1"><strong><?php //echo $tofollow['User']['full_name']; ?></strong></p> -->

                                    <?php
if ($peoples['User']['id'] != $this->Session->read('Auth.User.id')) {
        echo $this->Html->link($peoples['User']['full_name'], array('controller' => 'users', 'action' => 'view', $peoples['User']['id']), array('class' => 'font-weight-bold'));
    } else {
        echo $this->Html->link($peoples['User']['full_name'], array('controller' => 'users', 'action' => 'index'), array('class' => 'font-weight-bold'));

    }
    ?>
                                    <br>

                                    <i><?php echo "@" . $peoples['User']['username']; ?></i>


                                    <?php

    // if ($tofollow[0]['isFollowed'] == 1) {
    //     echo $this->Form->postLink('Unfollow', array('controller' => 'users', 'action' => 'unfollow', $tofollow['Follow']['id']),
    //         array('class' => 'btn btn-sm btn-outline-primary float-right'));
    // } else {
    //     echo $this->Form->postLink('Follow', array('controller' => 'users', 'action' => 'follow', $tofollow['User']['id']),
    //         array('class' => 'btn btn-sm btn-outline-primary float-right'));
    // } ?>

                                </div>

                            </div>
                            <hr class="my-2">
                            <?php endforeach; ?>
                            <?php unset($tofollow);

} else {

    echo '<div class="text-muted"><small>NO MATCHES FOUND <br> Please try another search.</small></div>';

}

?>

                        </div>

                        <?php

$paginator = $this->Paginator;

echo "<center><div class='paging'>";

echo $paginator->first('First');
echo " ";

if ($paginator->hasPrev()) {
    echo $paginator->prev('<<');
}
echo " ";
echo $paginator->numbers(array('modulus' => 2));
echo " ";

if ($paginator->hasNext()) {
    echo $paginator->next('>>');
}

echo " ";
echo $paginator->last('Last');
echo "</div>";
echo '<hr class="my-2">';

?>

<?= $this->element('footer'); ?>

</center>


                    </div>

                </div>




            </div>
        </div>
    </div>
    </div>

  

    </div>

</body>