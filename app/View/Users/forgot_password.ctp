<section>
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-12 col-md-8 col-lg-8 col-xl-6">
          <div class="row">
            <div class="col text-center">
              <h1>Password reset</h1>
              <p class="text-h3">Enter your email.</p>
            </div>
          </div>
          <?php echo $this->Form->create('User'); ?>
          
          <div class="row align-items-center mt-4">
            <div class="col">
              <input type="email" class="form-control" placeholder="Email" name="data[User][email]">
            </div>
          </div>
         
          <div class="alert alert-warning alert-dismissible" id="error_password" style="display:none;">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo "The password you entered did not match."; ?>
            </div>

          <div class="row align-items-center mt-4">
            <div class="col">

            <?php echo $this->Form->button(__('Done'), ['class' => 'form-control btn btn-primary', 'type' => 'submit', 'id' => 'btn_signup']); ?>

            </div>
          </div>

          <?php echo $this->Form->end(); ?>
          <center>
                 <div class="row justify-content-start mt-4">
                          <div class="col">
                          <div class="form-check">
                              <label class="form-check-label">
                              Already have an account?
                              <?=$this->Html->link("Sign in",
    array(
        'controller' => 'users',
        'action' => 'login',
    ))?>
                                     to Microblog
                              </label>
                          </div>
                          </div>
                  </div>
            <center>
        </div>
      </div>
    </div>
  </section>

  <script>

  // $(document).ready(function()
  //   {
  //       $("#error_password").hide();
  //   });

    $('#password2').on('keyup', function() {
      if($('#password1').val() != this.value){
        $("#error_password").show();
        $(':input[type="submit"]').prop('disabled', true);
      }else{
        $("#error_password").hide();
        $(':input[type="submit"]').prop('disabled', false);
      }
    });

    $('#password1').on('keyup', function() {
    if($('#password2').val() != ""){
          if($('#password2').val() != this.value){
            $("#error_password").show();
            $(':input[type="submit"]').prop('disabled', true);
          }else{
            $("#error_password").hide();
            $(':input[type="submit"]').prop('disabled', false);
          }
        }
    });


</script>



