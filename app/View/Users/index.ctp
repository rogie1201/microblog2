<body>

    <?=$this->element('leftnav')?>
    <?=$this->element('rightnav')?>

    <div class="container">
        <div class="container mt-4 mb-5">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-body">
                        <div class="e-profile">
                            <div class="row">
                                <div class="col-12 col-sm-auto mb-3">
                                    <div class="mx-auto" style="width: 140px;">
                                        <div class="d-flex justify-content-center align-items-center rounded"
                                            style="height: 140px; background-color: rgb(233, 236, 239);">

                                            <img src="<?php echo $this->webroot . "img/" . $this->Session->read('Auth.User.image'); ?>"
                                                class="img-responsive img-fluid" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col d-flex flex-column flex-sm-row justify-content-between mb-3">
                                    <div class="text-center text-sm-left mb-2 mb-sm-0">
                                        <h4 class="pt-sm-2 pb-1 mb-0 text-nowrap">
                                            <?php echo $this->Session->read('Auth.User.full_name'); ?></h4>
                                        <p class="mb-0">@<?php echo $this->Session->read('Auth.User.username'); ?></p>
                                        <div class="text-muted">
                                            <small><?php echo $this->Session->read('Auth.User.email'); ?></small>
                                        </div>
                                        <div class="text-muted"><small>Born
                                                <?php echo $this->Time->format($this->Session->read('Auth.User.birthdate'), '%B %e, %Y'); ?></small>
                                        </div>
                                        <div class="mt-2">

                                            <?php
                                            echo $this->Html->link('Edit Profile',
                                                array(
                                                    'controller' => 'users',
                                                    'action' => 'edit', $this->Session->read('Auth.User.id'),
                                                ),
                                                array(
                                                    'bootstrap-type' => 'primary',
                                                    'class' => 'btn btn-primary',
                                                    // transform link to a button
                                                    'rule' => 'button',
                                                )
                                            );
                                            ?>
                                        </div>

                                    </div>
                                    <div class="text-center text-sm-right">
                                        <div class="text-muted"><small>Joined
                                                <?php echo $this->Time->format($this->Session->read('Auth.User.created'), '%B %e, %Y'); ?></small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                                echo $this->Html->link(
                                    $this->Html->tag('i', '', array('class' => 'fas fa-user-friends')) .
                                    $this->Html->tag('span', ' Followers(' . count($followers) . ')'),
                                    array(
                                        'controller' => 'follows',
                                        'action' => 'followers', 1, $this->Session->read('Auth.User.id')
                                    ),
                                    array('escape' => false)
                                );

                                echo "&nbsp; &nbsp;" . $this->Html->link(
                                    $this->Html->tag('i', '', array('class' => 'fas fa-user-friends')) .
                                    $this->Html->tag('span', ' Following(' . count($followings) . ')'),
                                    array(
                                        'controller' => 'follows',
                                        'action' => 'following', 2, $this->Session->read('Auth.User.id')
                                    ),
                                    array('escape' => false)
                                );

                                ?>
                        </div>
                    </div>


                    <div class="feed p-2">

                        <!-- feed start -->

                        <?php

                        foreach ($posts as $post):

                        ?>

                        <!--Post-->

                        <?php if ($post['UserPostView']['POST_TYPE'] == 'REGULAR_POST') {?>

                        <div class="bg-white border mt-2">
                            <div class="d-flex flex-row justify-content-between align-items-center p-2 border-bottom">
                                <div class="d-flex flex-row align-items-center feed-text px-2"><img
                                        class="rounded-circle"
                                        src="<?php echo $this->webroot . "img/" . $post['UserPostView']['USER_IMAGE']; ?>"
                                        width="45">
                                    <div class="d-flex flex-column flex-wrap ml-2">

                                        <?php
                                            if ($post['UserPostView']['USER_ID'] != $this->Session->read('Auth.User.id')) {
                                                echo $this->Html->link($post['UserPostView']['FULL_NAME'], array('controller' => 'users', 'action' => 'view', $post['UserPostView']['USER_ID']), array('class' => 'font-weight-bold'));
                                            } else {
                                                echo $this->Html->link($post['UserPostView']['FULL_NAME'], array('controller' => 'users', 'action' => 'index'), array('class' => 'font-weight-bold'));

                                            }
                                                ?> <span
                                            class="text-black-50 time"><?php echo $this->Time->format($post['UserPostView']['MODIFIED'], '%B %e, %Y %H:%M %p'); ?></span>
                                    </div>
                                </div>

                                <div class="d-flex justify-content-end socials p-2 py-3">
                                    <?php if ($post['UserPostView']['USER_ID'] == $this->Session->read('Auth.User.id')) { ?>
                                    <div class="card-actions float-right">
                                        <div class="dropdown">
                                            <button class="btn btn-default" type="button" id="dropdownMenuButton"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                    fill="currentColor" class="bi bi-three-dots-vertical"
                                                    viewBox="0 0 16 16">
                                                    <path
                                                        d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z">
                                                    </path>
                                                </svg>
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

                                                <?php echo $this->Html->link('Edit', array('controller'=>'posts', 'action' => 'edit', $post['UserPostView']['POST_ID']), array('class' => 'dropdown-item')); ?>
                                                <?php echo $this->Form->postLink('Delete', array('controller'=>'posts','action' => 'delete', $post['UserPostView']['POST_ID']),
                                                        array('class' => 'dropdown-item', 'confirm' => 'Are you sure you want to delete this?')); 
                                                        ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>

                            </div>



                            <!--CONTENT DIV-->
                            <div onclick="location.href='<?= $this->webroot?>posts/view/<?=$post['UserPostView']['POST_ID']?>'"
                                style="cursor:pointer;">
                                <div class="p-2 px-3">
                                    <span><?php echo h($post['UserPostView']['CONTENT']); ?></span>
                                </div>
                                <?php if ($post['UserPostView']['IMAGE'] != null) { ?>
                                <div class="feed-image p-2 px-3">
                                    <img class="img-fluid img-responsive"
                                        src="<?php echo $this->webroot . "img/" . $post['UserPostView']['IMAGE']; ?>">
                                </div>
                                <?php } ?>
                            </div>
                            <!--CONTENT DIV END-->


                            <section class="post-footer">
                                <hr>
                                <div class="post-footer-option container">
                                    <ul class="list-unstyled">
                                        <?php
                                                        if ($post['UserPostView']['IS_LIKED'] == 1) {
                                                            echo $this->Html->tag('i', 'Unlike', array('class' => 'fa fa-thumbs-up post_liked', 'id' => 'post_unlike_' . $post['UserPostView']['POST_ID'] . '', 'onclick' => 'unlike(' . $post['UserPostView']['POST_ID'] . ');'));
                                                            echo $this->Html->tag('i', 'Like', array('class' => 'fa fa-thumbs-up display_none', 'id' => 'post_like_' . $post['UserPostView']['POST_ID'] . '', 'onclick' => 'like(' . $post['UserPostView']['POST_ID'] . ');'));
                                                        } else {
                                                            echo $this->Html->tag('i', 'Unlike', array('class' => 'fa fa-thumbs-up post_liked display_none', 'id' => 'post_unlike_' . $post['UserPostView']['POST_ID'] . '', 'onclick' => 'unlike(' . $post['UserPostView']['POST_ID'] . ');'));
                                                            echo $this->Html->tag('i', 'Like', array('class' => 'fa fa-thumbs-up', 'id' => 'post_like_' . $post['UserPostView']['POST_ID'] . '', 'onclick' => 'like(' . $post['UserPostView']['POST_ID'] . ');'));
                                                        }
                                                    ?>
                                        <i
                                            id="post_like_count_<?php echo $post['UserPostView']['POST_ID']; ?>"><?php echo $post['UserPostView']['LIKES_COUNT'] == 0 ? "" : $post['UserPostView']['LIKES_COUNT']; ?></i>

                                        <i class="p-3"></i>

                                        <?php echo $this->Html->tag('i', 'Comment', array('class' => 'fas fa-comments post_liked', 'id' => 'post_comment_' . $post['UserPostView']['POST_ID'] . '', 'onclick' => 'loadComment(' . $post['UserPostView']['POST_ID'] . ')')); ?>
                                        <i id="post_comment_count_<?=$post['UserPostView']['POST_ID'] ?>"><?php echo $post['UserPostView']['COMMENTS_COUNT'] == 0 ? "" : $post['UserPostView']['COMMENTS_COUNT'] ?></i>

                                        <i class="p-3"></i>

                                        <?php
                                                        echo $this->Form->postLink('Share', array('controller'=>'posts', 'action' => 'repost',  $post['UserPostView']['POST_ID']),
                                                                array('class' => 'fas fa-share')
                                                            );
                                                            ?>

                                        <!-- <i class="fa fa-share">Share</i> -->


                                    </ul>
                                </div>
                            </section>

                            <!--START OF DISPLAY COMMENTS HERE-->
                            <div id="divLoadComment_<?= $post['UserPostView']['POST_ID'] ?>"></div>
                            <!--END OF DISPLAY COMMENTS HERE-->


                        </div>

                        <!--REPOST-->

                        <?php } else if ($post['UserPostView']['POST_TYPE'] == 'SHARED_POST') {?>
                        <div class="bg-white border mt-2">
                            <div>
                                <div
                                    class="d-flex flex-row justify-content-between align-items-center p-2 border-bottom">
                                    <div class="d-flex flex-row align-items-center feed-text px-2"><img
                                            class="rounded-circle"
                                            src="<?php echo $this->webroot . "img/" . $post['UserPostView']['USER_IMAGE']; ?>"
                                            width="45">
                                        <div class="d-flex flex-column flex-wrap ml-2">

                                            <?php
                                                if ($post['UserPostView']['USER_ID'] != $this->Session->read('Auth.User.id')) {
                                                    echo $this->Html->link($post['UserPostView']['FULL_NAME'], array('controller' => 'users', 'action' => 'view', $post['UserPostView']['USER_ID']), array('class' => 'font-weight-bold'));
                                                } else {
                                                    echo $this->Html->link($post['UserPostView']['FULL_NAME'], array('controller' => 'users', 'action' => 'index'), array('class' => 'font-weight-bold'));

                                                }
                                                    ?>
                                            <span
                                                class="text-black-50 time"><?php echo $this->Time->format($post['UserPostView']['MODIFIED'], '%B %e, %Y %H:%M %p'); ?></span>
                                        </div>
                                    </div>


                                    <div class="d-flex justify-content-end socials p-2 py-3">
                                        <?php if ($post['UserPostView']['USER_ID'] == $this->Session->read('Auth.User.id')) { ?>
                                        <div class="card-actions float-right">
                                            <div class="dropdown">
                                                <button class="btn btn-default" type="button" id="dropdownMenuButton"
                                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                        fill="currentColor" class="bi bi-three-dots-vertical"
                                                        viewBox="0 0 16 16">
                                                        <path
                                                            d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z">
                                                        </path>
                                                    </svg>
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

                                                    <?php //echo $this->Html->link('Edit', array('action' => 'edit', $post['PostView']['POST_ID']), array('class' => 'dropdown-item')); ?>
                                                    <?php echo $this->Form->postLink('Delete', array('controller'=>'posts', 'action' => 'delete', $post['UserPostView']['POST_ID']),
                                                                array('class' => 'dropdown-item', 'confirm' => 'Are you sure you want to delete this?')); 
                                                                ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>



                                </div>
                            </div>

                            <div class="p-2 px-3">

                                <div onclick="location.href='<?= $this->webroot?>posts/view/<?=$post['UserPostView']['POST_ID']?>'"
                                    style="cursor:pointer;">

                                    <!--Original Post-->
                                    <?php if($post['UserPostView']['IS_DELETED']==1){
                                       echo '<div class="text-muted"><small>Content not available or maybe deleted.</small></div>';
                                        } else {
                                       ?>
                                    <div class="bg-white border mt-2">
                                        <div>
                                            <div
                                                class="d-flex flex-row justify-content-between align-items-center p-2 border-bottom">
                                                <div class="d-flex flex-row align-items-center feed-text px-2">
                                                    <img class="rounded-circle"
                                                        src="<?php echo $this->webroot . "img/" . $post['UserPostView']['ORG_USER_IMAGE']; ?>"
                                                        width="45">
                                                    <div class="d-flex flex-column flex-wrap ml-2">


                                                        <?php
                                                                        if ($post['UserPostView']['ORG_USER_ID'] != $this->Session->read('Auth.User.id')) {
                                                                                echo $this->Html->link($post['UserPostView']['ORG_NAME'], array('controller' => 'users', 'action' => 'view', $post['UserPostView']['ORG_USER_ID']), array('class' => 'font-weight-bold'));
                                                                            } else {
                                                                                echo $this->Html->link($post['UserPostView']['ORG_NAME'], array('controller' => 'users', 'action' => 'index'), array('class' => 'font-weight-bold'));
                                                                            }
                                                                            ?>

                                                        <span
                                                            class="text-black-50 time"><?php echo $this->Time->format($post['UserPostView']['ORG_MODIFIED'], '%B %e, %Y %H:%M %p'); ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="p-2 px-3">
                                            <span><?php echo h($post['UserPostView']['CONTENT']); ?></span>
                                        </div>

                                        <?php if ($post['UserPostView']['IMAGE'] != null) { ?>
                                        <div class="feed-image p-2 px-3">
                                            <img class="img-fluid img-responsive"
                                                src="<?php echo $this->webroot . "img/" . $post['UserPostView']['IMAGE']; ?>">
                                        </div>
                                        <?php } ?>
                                    </div>
                                    <?php } ?>
                                    <!--End Original Post-->
                                </div>
                            </div>

                            <section class="post-footer">
                                <hr>
                                <div class="post-footer-option container">
                                    <ul class="list-unstyled">
                                        <?php
                                                        if ($post['UserPostView']['IS_LIKED'] == 1) {
                                                            echo $this->Html->tag('i', 'Unlike', array('class' => 'fa fa-thumbs-up post_liked', 'id' => 'post_unlike_' . $post['UserPostView']['POST_ID'] . '', 'onclick' => 'unlike(' . $post['UserPostView']['POST_ID'] . ');'));
                                                            echo $this->Html->tag('i', 'Like', array('class' => 'fa fa-thumbs-up display_none', 'id' => 'post_like_' . $post['UserPostView']['POST_ID'] . '', 'onclick' => 'like(' . $post['UserPostView']['POST_ID'] . ');'));
                                                            } else {
                                                                echo $this->Html->tag('i', 'Unlike', array('class' => 'fa fa-thumbs-up post_liked display_none', 'id' => 'post_unlike_' . $post['UserPostView']['POST_ID'] . '', 'onclick' => 'unlike(' . $post['UserPostView']['POST_ID'] . ');'));
                                                                echo $this->Html->tag('i', 'Like', array('class' => 'fa fa-thumbs-up', 'id' => 'post_like_' . $post['UserPostView']['POST_ID'] . '', 'onclick' => 'like(' . $post['UserPostView']['POST_ID'] . ');'));
                                                            }
                                                        ?>
                                        <i
                                            id="post_like_count_<?php echo $post['UserPostView']['POST_ID']; ?>"><?php echo $post['UserPostView']['LIKES_COUNT'] == 0 ? "" : $post['UserPostView']['LIKES_COUNT']; ?></i>
                                        <i class="p-3"></i>
                                        <?php echo $this->Html->tag('i', 'Comment', array('class' => 'fas fa-comments post_liked', 'id' => 'post_comment_' . $post['UserPostView']['POST_ID'] . '', 'onclick' => 'loadComment(' . $post['UserPostView']['POST_ID'] . ')')); ?>
                                        <i id="post_comment_count_<?=$post['UserPostView']['POST_ID'] ?>"><?php echo $post['UserPostView']['COMMENTS_COUNT'] == 0 ? "" : $post['UserPostView']['COMMENTS_COUNT'] ?></i>
                                        <i class="p-3"></i>
                                        <?php
                                                        echo $this->Form->postLink('Share', array('controller'=>'posts', 'action' => 'repost',  $post['UserPostView']['POST_ID']),
                                                                array('class' => 'fas fa-share')
                                                            );
                                                            ?>

                                        <!-- <i class="fa fa-share">Share</i> -->
                                    </ul>
                                </div>
                            </section>


                            <!--START OF DISPLAY COMMENTS HERE-->
                            <div id="divLoadComment_<?= $post['UserPostView']['POST_ID'] ?>"></div>
                            <!--END OF DISPLAY COMMENTS HERE-->

                        </div>

                        <?php }?>

                        <?php endforeach;?>
                        <?php unset($post);?>

                        <!-- feed end -->

                    </div>


                    <?php

$paginator = $this->Paginator;

echo "<center><div class='paging'>";

echo $paginator->first('First');
echo " ";

if ($paginator->hasPrev()) {
    echo $paginator->prev('<<');
}
echo " ";
echo $paginator->numbers(array('modulus' => 2));
echo " ";

if ($paginator->hasNext()) {
    echo $paginator->next('>>');
}

echo " ";
echo $paginator->last('Last');

echo "</div>";
echo '<hr class="my-2">';

?>
                    <?= $this->element('footer'); ?>

                    </center>


                </div>
            </div>
        </div>
    </div>


</body>