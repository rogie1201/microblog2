<section>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-8 col-lg-8 col-xl-6">
                <div class="row">
                    <div class="col text-center">
                        <h1>Create your account</h1>
                        <p class="text-h3">You are creating an account to a simple microblog.</p>
                    </div>
                </div>



                <?php    
                echo $this->Form->create('User');
                 ?>


                <div class="row align-items-center">
                    <div class="col mt-4">
                        <label for="full_name" class="control-label">Name*</label>
                        <input type="text" class="form-control" placeholder="Name" name="data[User][full_name]"
                            value="<?=(isset($data['User']['full_name'])) ? h($data['User']['full_name']) : ''; ?>">
                    </div>
                </div>
                <?php if (isset($errors['full_name'])): ?>
                <div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?=h($errors['full_name'][0]) ?>
                </div>
                <?php endif ?>

                <div class="row align-items-center mt-4">
                    <div class="col">
                        <label for="email" class="control-label">Email*</label>
                        <input type="email" class="form-control" placeholder="Email" name="data[User][email]"
                            value="<?=(isset($data['User']['email'])) ? h($data['User']['email']) : ''; ?>">
                    </div>
                </div>
                <?php if (isset($errors['email'])): ?>
                <div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?=h($errors['email'][0]) ?>
                </div>
                <?php endif ?>


                <div class="row align-items-center mt-4">
                    <div class="col">
                        <label for="birthDate" class="control-label">Date of Birth*</label>
                        <input type="date" class="form-control" placeholder="Birthdate" name="data[User][birthdate]"
                            title="Birthdate"
                            value="<?=(isset($data['User']['birthdate'])) ? h($data['User']['birthdate']) : ''; ?>">
                    </div>
                </div>
                <?php if (isset($errors['birthdate'])): ?>
                <div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?=h($errors['birthdate'][0]) ?>
                </div>
                <?php endif ?>

                <div class="row align-items-center">
                    <div class="col mt-4">
                        <label for="username" class="control-label">Username*</label>
                        <input type="text" class="form-control" placeholder="Username" name="data[User][username]"
                            value="<?=(isset($data['User']['username'])) ? h($data['User']['username']) : ''; ?>">
                    </div>
                </div>
                <?php if (isset($errors['username'])): ?>
                <div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?=h($errors['username'][0]) ?>
                </div>
                <?php endif ?>


                <div class="row align-items-center mt-4">
                    <div class="col">
                        <label for="username" class="control-label">Password*</label>
                        <input type="password" class="form-control" placeholder="Password" name="data[User][password]"
                            id="password1"
                            value="<?=(isset($data['User']['password'])) ? h($data['User']['password']) : ''; ?>">
                    </div>
                    <div class="col">
                        <label for="username" class="control-label">Confirm Password*</label>
                        <input type="password" class="form-control" placeholder="Confirm Password" id="password2">
                    </div>
                </div>
                <?php if (isset($errors['password'])): ?>
                <div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?=h($errors['password'][0]) ?>
                </div>
                <?php endif ?>


                <div class="alert alert-warning alert-dismissible" id="error_password" style="display:none;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo "The password you entered did not match."; ?>
                </div>

                <div class="row align-items-center mt-4">
                    <div class="col">

                        <?php echo $this->Form->button(__('Sign up'), ['class' => 'form-control btn btn-primary', 'type' => 'submit', 'id' => 'btn_signup']); ?>

                    </div>
                </div>

                <?php echo $this->Form->end(); ?>
                <center>
                    <div class="row justify-content-start mt-4">
                        <div class="col">
                            <div class="form-check">
                                <label class="form-check-label">
                                    Already have an account?
                                    <?=$this->Html->link("Sign in",
    array(
        'controller' => 'users',
        'action' => 'login',
    )) ?>
                                    to Microblog
                                </label>
                            </div>
                        </div>
                    </div>
                    <center>
            </div>
        </div>
    </div>
</section>


<script>
// $(document).ready(function()
//   {
//       $("#error_password").hide();
//   });

$('#password2').on('keyup', function() {
    if ($('#password1').val() != this.value) {
        $("#error_password").show();
        $(':input[type="submit"]').prop('disabled', true);
    } else {
        $("#error_password").hide();
        $(':input[type="submit"]').prop('disabled', false);
    }
});

$('#password1').on('keyup', function() {
    if ($('#password2').val() != "") {
        if ($('#password2').val() != this.value) {
            $("#error_password").show();
            $(':input[type="submit"]').prop('disabled', true);
        } else {
            $("#error_password").hide();
            $(':input[type="submit"]').prop('disabled', false);
        }
    }

    baseUrl();


});
</script>