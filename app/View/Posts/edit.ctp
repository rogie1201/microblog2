<div class="" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modify Post</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <?php foreach ($posts as $post): ?>

                <form method="POST" action="<?=$this->base ?>/posts/edit/<?=$post['id'] ?>"
                    enctype="multipart/form-data">

                    <div class="card-body">
                        <div class="form-group">

                            <textarea class="form-control form-control-lg" rows="2" name="post_txt"
                                placeholder="What's happening?"><?php echo $post['content']; ?></textarea>
                        </div>

                        <div class="card-footer">
                            <label class="btn btn-default">
                                <i class="fas fa-image"></i>
                                Insert Image <input type="file" id="imgInp" name="postImage" hidden>
                            </label>
                            <br>
                            <?php if ($post['image'] != null) {echo '<input id="has_image" style="display:none;" value="1" />';} else {echo '<input id="has_image" style="display:none;" value="0" />';} ?>

                            <div class="feed-image p-2 px-3">
                                <img id="imgPost" src="<?php echo $this->webroot ."img/" . $post['image']; ?>"
                                    class="img-fluid" style="display:none;" />
                            </div>

                        </div>

                        <?php endforeach; ?>

                    </div>
                    <div class="modal-footer">
                        <?php
                        echo $this->Html->link('Back',
                            array(
                                'controller' => 'posts',
                                'action' => 'index',
                            ),
                            array(
                                'bootstrap-type' => 'primary',
                                'class' => 'btn btn-secondary',
                                'data-dismiss' => 'modal',
                                // transform link to a button
                                'rule' => 'button',
                            )
                        );
                        ?>

                        <button type="submit" class="btn btn-primary">Save changes</button>
                </form>
                <?php
                if ($post['image'] != null) {
                    echo $this->Form->postLink('Remove Image', array('action' => 'deleteImage', $post['id']),
                        array('class' => 'btn btn-primary', 'id' => 'removeImgFile', 'confirm' => 'Are you sure you want to delete this?')
                    );
                } else {
                    echo '<a class="btn btn-primary" id="removeImgFile" style="color:white;">Remove Image</a>';
                }
?>

            </div>
        </div>
    </div>
</div>
<script>
$("#removeImgFile").click(function() {
    $("#imgPost").val(null);
    $('#imgPost').attr('src', "#");
    $("#imgPost").hide();
    $("#removeImgFile").hide();
    $("#imgInp").val("");
});

hasImage();

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#imgPost').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);

        $("#imgPost").show();
        $("#removeImgFile").show();
    }
}

$("#imgInp").change(function() {
    readURL(this);
});

function hasImage() {

    if ($("#has_image").val() == 1) {
        $("#imgPost").show();
        $("#removeImgFile").show();
    } else {
        $("#imgPost").hide();
        $("#removeImgFile").hide();
    }

}
</script>