<body>

    <?=$this->element('leftnav') ?>
    <?=$this->element('rightnav') ?>

    <div class="container">
        <div class="container mt-4 mb-5">
            <div class="d-flex justify-content-center row">
                <div class="col-md-12">
                    <div class="feed p-2">

                        <div class="card card-primary">

                            <!-- form start -->
                            <form method="POST" action="<?=$this->base ?>/posts/add" enctype="multipart/form-data">

                                <div class="card-body">
                                    <div class="form-group">

                                        <textarea class="form-control form-control-lg" rows="2" name="post_txt"
                                            placeholder="What's happening?"></textarea>
                                    </div>

                                    <div class="card-footer">
                                        <div class="float-right">
                                            <button type="submit" class="btn btn-primary"><i class="far fa-write"></i>
                                                Post</button>
                                        </div>
                                        <label class="btn btn-default">
                                            <i class="fas fa-image"></i>
                                            Insert Image <input type="file" id="imgInp" name="postImage" hidden>
                                        </label>
                                        <br>
                                        <br>

                                        <div class="feed-image p-2 px-3">
                                            <img id="imgPost" src="#" class="img-fluid res" style="display:none;" />
                                            <a class="remove-image" id="removeImgFile" href="#"
                                                style="display: none;">&#215;</a>
                                        </div>


                                    </div>
                            </form>
                            <!-- form end -->
                        </div>

                        <!-- feed start -->

                        <?php foreach ($posts as $post): ?>

                        <!--POST-->

                        <?php if ($post['PostView']['POST_TYPE'] == 'REGULAR_POST') { ?>
                        <div class="bg-white border mt-2">
                            <div class="d-flex flex-row justify-content-between align-items-center p-2 border-bottom">
                                <div class="d-flex flex-row align-items-center feed-text px-2"><img
                                        class="rounded-circle"
                                        src="<?php echo $this->webroot . "img/" . $post['PostView']['USER_IMAGE']; ?>"
                                        width="45">
                                    <div class="d-flex flex-column flex-wrap ml-2">

                                        <?php
                                            if ($post['PostView']['USER_ID'] != $this->Session->read('Auth.User.id')) {
                                                echo $this->Html->link($post['PostView']['FULL_NAME'], array('controller' => 'users', 'action' => 'view', $post['PostView']['USER_ID']), array('class' => 'font-weight-bold'));
                                            } else {
                                                echo $this->Html->link($post['PostView']['FULL_NAME'], array('controller' => 'users', 'action' => 'index'), array('class' => 'font-weight-bold'));

                                            }
                                                ?> <span
                                            class="text-black-50 time"><?php echo $this->Time->format($post['PostView']['MODIFIED'], '%B %e, %Y %H:%M %p'); ?></span>
                                    </div>
                                </div>

                                <div class="d-flex justify-content-end socials p-2 py-3">
                                    <?php if ($post['PostView']['USER_ID'] == $this->Session->read('Auth.User.id')) { ?>
                                    <div class="card-actions float-right">
                                        <div class="dropdown">
                                            <button class="btn btn-default" type="button" id="dropdownMenuButton"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                    fill="currentColor" class="bi bi-three-dots-vertical"
                                                    viewBox="0 0 16 16">
                                                    <path
                                                        d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z">
                                                    </path>
                                                </svg>
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

                                                <?php echo $this->Html->link('Edit', array('action' => 'edit', $post['PostView']['POST_ID']), array('class' => 'dropdown-item')); ?>
                                                <?php echo $this->Form->postLink('Delete', array('action' => 'delete', $post['PostView']['POST_ID']),
                                                        array('class' => 'dropdown-item', 'confirm' => 'Are you sure you want to delete this?')); 
                                                        ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>

                            </div>



                            <!--CONTENT DIV-->
                            <div onclick="location.href='<?= $this->webroot?>posts/view/<?=$post['PostView']['POST_ID']?>'"
                                style="cursor:pointer;">
                                <div class="p-2 px-3"><span><?php echo h($post['PostView']['CONTENT']); ?></span></div>
                                <?php if ($post['PostView']['IMAGE'] != null) { ?>
                                <div class="feed-image p-2 px-3">
                                    <img class="img-fluid img-responsive"
                                        src="<?php echo $this->webroot . "img/" . $post['PostView']['IMAGE']; ?>">
                                </div>
                                <?php } ?>
                            </div>
                            <!--CONTENT DIV END-->


                            <section class="post-footer">
                                <hr>
                                <div class="post-footer-option container">
                                    <ul class="list-unstyled">
                                        <?php
                                      if ($post['PostView']['IS_LIKED'] == 1) {
                                        echo $this->Html->tag('i', 'Unlike', array('class' => 'fa fa-thumbs-up post_liked', 'id' => 'post_unlike_' . $post['PostView']['POST_ID'] . '', 'onclick' => 'unlike(' . $post['PostView']['POST_ID'] . ');'));
                                        echo $this->Html->tag('i', 'Like', array('class' => 'fa fa-thumbs-up display_none', 'id' => 'post_like_' . $post['PostView']['POST_ID'] . '', 'onclick' => 'like(' . $post['PostView']['POST_ID'] . ');'));
                                    } else {
                                        echo $this->Html->tag('i', 'Unlike', array('class' => 'fa fa-thumbs-up post_liked display_none', 'id' => 'post_unlike_' . $post['PostView']['POST_ID'] . '', 'onclick' => 'unlike(' . $post['PostView']['POST_ID'] . ');'));
                                        echo $this->Html->tag('i', 'Like', array('class' => 'fa fa-thumbs-up', 'id' => 'post_like_' . $post['PostView']['POST_ID'] . '', 'onclick' => 'like(' . $post['PostView']['POST_ID'] . ');'));
                                    }
                                   ?>
                                        <i
                                            id="post_like_count_<?php echo $post['PostView']['POST_ID']; ?>"><?php echo $post['PostView']['LIKES_COUNT'] == 0 ? "" : $post['PostView']['LIKES_COUNT']; ?></i>

                                        <i class="p-3"></i>

                                        <?php echo $this->Html->tag('i', 'Comment', array('class' => 'fas fa-comments post_liked', 'id' => 'post_comment_' . $post['PostView']['POST_ID'] . '', 'onclick' => 'loadComment(' . $post['PostView']['POST_ID'] . ')')); ?>
                                        <i id="post_comment_count_<?=$post['PostView']['POST_ID'] ?>"><?php echo $post['PostView']['COMMENTS_COUNT'] == 0 ? "" : $post['PostView']['COMMENTS_COUNT'] ?></i>

                                        <i class="p-3"></i>

                                        <?php
                                          echo $this->Form->postLink('Share', array('action' => 'repost',  $post['PostView']['POST_ID']),
                                                array('class' => 'fas fa-share')
                                            );
                                            ?>

                                        <!-- <i class="fa fa-share">Share</i> -->


                                    </ul>
                                </div>
                            </section>

                            <!--START OF DISPLAY COMMENTS HERE-->
                            <div id="divLoadComment_<?= $post['PostView']['POST_ID'] ?>"></div>
                            <!--END OF DISPLAY COMMENTS HERE-->


                        </div>


                        <!--REPOST-->



                        <?php } elseif ($post['PostView']['POST_TYPE'] == 'SHARED_POST') { ?>


                        <div class="bg-white border mt-2">
                            <div>
                                <div
                                    class="d-flex flex-row justify-content-between align-items-center p-2 border-bottom">
                                    <div class="d-flex flex-row align-items-center feed-text px-2"><img
                                            class="rounded-circle"
                                            src="<?php echo $this->webroot . "img/" . $post['PostView']['USER_IMAGE']; ?>"
                                            width="45">
                                        <div class="d-flex flex-column flex-wrap ml-2">

                                            <?php
                                                if ($post['PostView']['USER_ID'] != $this->Session->read('Auth.User.id')) {
                                                    echo $this->Html->link($post['PostView']['FULL_NAME'], array('controller' => 'users', 'action' => 'view', $post['PostView']['USER_ID']), array('class' => 'font-weight-bold'));
                                                } else {
                                                    echo $this->Html->link($post['PostView']['FULL_NAME'], array('controller' => 'users', 'action' => 'index'), array('class' => 'font-weight-bold'));

                                                }
                                                    ?>
                                            <span
                                                class="text-black-50 time"><?php echo $this->Time->format($post['PostView']['MODIFIED'], '%B %e, %Y %H:%M %p'); ?></span>
                                        </div>
                                    </div>


                                    <div class="d-flex justify-content-end socials p-2 py-3">
                                        <?php if ($post['PostView']['USER_ID'] == $this->Session->read('Auth.User.id')) { ?>
                                        <div class="card-actions float-right">
                                            <div class="dropdown">
                                                <button class="btn btn-default" type="button" id="dropdownMenuButton"
                                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                        fill="currentColor" class="bi bi-three-dots-vertical"
                                                        viewBox="0 0 16 16">
                                                        <path
                                                            d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z">
                                                        </path>
                                                    </svg>
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

                                                    <?php //echo $this->Html->link('Edit', array('action' => 'edit', $post['PostView']['POST_ID']), array('class' => 'dropdown-item')); ?>
                                                    <?php echo $this->Form->postLink('Delete', array('action' => 'delete', $post['PostView']['POST_ID']),
                                                                array('class' => 'dropdown-item', 'confirm' => 'Are you sure you want to delete this?')); 
                                                                ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>



                                </div>
                            </div>

                            <div class="p-2 px-3">

                                <div onclick="location.href='<?= $this->webroot?>posts/view/<?=$post['PostView']['POST_ID']?>'"
                                    style="cursor:pointer;">

                                    <!--Original Post-->

                                    <?php if($post['PostView']['IS_DELETED']==1){
                                                         echo '<div class="text-muted"><small>Content not available or maybe deleted.</small></div>';
                                                        } else {
                                       ?>
                                    <div class="bg-white border mt-2">
                                        <div>
                                            <div
                                                class="d-flex flex-row justify-content-between align-items-center p-2 border-bottom">
                                                <div class="d-flex flex-row align-items-center feed-text px-2"><img
                                                        class="rounded-circle"
                                                        src="<?php echo $this->webroot . "img/" . $post['PostView']['ORG_USER_IMAGE']; ?>"
                                                        width="45">
                                                    <div class="d-flex flex-column flex-wrap ml-2">


                                                        <?php
                                                    if ($post['PostView']['ORG_USER_ID'] != $this->Session->read('Auth.User.id')) {
                                                            echo $this->Html->link($post['PostView']['ORG_NAME'], array('controller' => 'users', 'action' => 'view', $post['PostView']['ORG_USER_ID']), array('class' => 'font-weight-bold'));
                                                        } else {
                                                            echo $this->Html->link($post['PostView']['ORG_NAME'], array('controller' => 'users', 'action' => 'index'), array('class' => 'font-weight-bold'));
                                                        }
                                                        ?>

                                                        <span
                                                            class="text-black-50 time"><?php echo $this->Time->format($post['PostView']['ORG_MODIFIED'], '%B %e, %Y %H:%M %p'); ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="p-2 px-3">
                                            <span><?php echo h($post['PostView']['CONTENT']); ?></span>
                                        </div>

                                        <?php if ($post['PostView']['IMAGE'] != null) { ?>
                                        <div class="feed-image p-2 px-3">
                                            <img class="img-fluid img-responsive"
                                                src="<?php echo $this->webroot . "img/" . $post['PostView']['IMAGE']; ?>">
                                        </div>
                                        <?php } ?>

                                    </div>
                                    <?php } ?>
                                    <!--End Original Post-->
                                </div>
                            </div>

                            <section class="post-footer">
                                <hr>
                                <div class="post-footer-option container">
                                    <ul class="list-unstyled">
                                        <?php
                                      if ($post['PostView']['IS_LIKED'] == 1) {
                                        echo $this->Html->tag('i', 'Unlike', array('class' => 'fa fa-thumbs-up post_liked', 'id' => 'post_unlike_' . $post['PostView']['POST_ID'] . '', 'onclick' => 'unlike(' . $post['PostView']['POST_ID'] . ');'));
                                        echo $this->Html->tag('i', 'Like', array('class' => 'fa fa-thumbs-up display_none', 'id' => 'post_like_' . $post['PostView']['POST_ID'] . '', 'onclick' => 'like(' . $post['PostView']['POST_ID'] . ');'));
                                        } else {
                                            echo $this->Html->tag('i', 'Unlike', array('class' => 'fa fa-thumbs-up post_liked display_none', 'id' => 'post_unlike_' . $post['PostView']['POST_ID'] . '', 'onclick' => 'unlike(' . $post['PostView']['POST_ID'] . ');'));
                                            echo $this->Html->tag('i', 'Like', array('class' => 'fa fa-thumbs-up', 'id' => 'post_like_' . $post['PostView']['POST_ID'] . '', 'onclick' => 'like(' . $post['PostView']['POST_ID'] . ');'));
                                        }
                                       ?>
                                        <i
                                            id="post_like_count_<?php echo $post['PostView']['POST_ID']; ?>"><?php echo $post['PostView']['LIKES_COUNT'] == 0 ? "" : $post['PostView']['LIKES_COUNT']; ?></i>
                                        <i class="p-3"></i>
                                        <?php echo $this->Html->tag('i', 'Comment', array('class' => 'fas fa-comments post_liked', 'id' => 'post_comment_' . $post['PostView']['POST_ID'] . '', 'onclick' => 'loadComment(' . $post['PostView']['POST_ID'] . ')')); ?>
                                        <i id="post_comment_count_<?=$post['PostView']['POST_ID'] ?>"><?php echo $post['PostView']['COMMENTS_COUNT'] == 0 ? "" : $post['PostView']['COMMENTS_COUNT'] ?></i>
                                        <i class="p-3"></i>
                                        <?php
                                          echo $this->Form->postLink('Share', array('action' => 'repost',  $post['PostView']['POST_ID']),
                                                array('class' => 'fas fa-share')
                                            );
                                            ?>

                                        <!-- <i class="fa fa-share">Share</i> -->
                                    </ul>
                                </div>
                            </section>


                            <!--START OF DISPLAY COMMENTS HERE-->
                            <div id="divLoadComment_<?= $post['PostView']['POST_ID'] ?>"></div>
                            <!--END OF DISPLAY COMMENTS HERE-->

                        </div>

                        <?php } ?>

                        <?php endforeach; ?>
                        <?php unset($post); ?>

                        <!-- feed end -->


                    </div>
                </div>
            </div>
        </div>

        <?php

$paginator = $this->Paginator;

echo "<center><div class='paging'>";

echo $paginator->first('First');
echo " ";

if ($paginator->hasPrev()) {
    echo $paginator->prev('<<');
}
echo " ";
echo $paginator->numbers(array('modulus' => 2));
echo " ";

if ($paginator->hasNext()) {
    echo $paginator->next('>>');
}

echo " ";
echo $paginator->last('Last');

echo "</div>";
echo '<hr class="my-2">';

?>
        <?=$this->element('footer'); ?>

        </center>




    </div>





</body>

<script>
$("#removeImgFile").click(function() {
    $("#imgPost").val(null);
    $('#imgPost').attr('src', "#");
    $("#imgPost").hide();
    $("#removeImgFile").hide();
    $("#imgInp").val("");
});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#imgPost').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);

        $("#imgPost").show();
        $("#removeImgFile").show();
    }
}

$("#imgInp").change(function() {
    readURL(this);
});
</script>