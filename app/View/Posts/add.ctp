        <?=$this->Form->create('Post', array(
    'enctype' => 'multipart/form-data',
    'id' => 'uploadForm',
))?>


        <div class="card-body">
            <div class="form-group">

                <textarea class="form-control form-control-lg" rows="2" name="post_txt"
                    placeholder="What's happening?"></textarea>
            </div>

            <div class="card-footer">
                <div class="float-right">
                    <button type="submit" class="btn btn-primary"><i class="far fa-write"></i>
                        Post</button>
                </div>
                <label class="btn btn-default">
                    <i class="fas fa-image"></i>
                    Insert Image <input type="file" id="imgInp" name="postImage" hidden>
                </label>
                <br>
                <img id="imgPost" src="#" class="img-fluid" style="display:none;" />
                <div class="float-left">
                    <input type="button" id="removeImgFile" class="btn btn-primary" value="Remove"
                        style="display:none;" />
                </div>
            </div>
            <?php echo $this->Form->end(); ?>