<?=$this->element('leftnav')?>
<?=$this->element('rightnav')?>


<div class="container">
    <div class="container mt-4 mb-5">
        <div class="d-flex justify-content-center row">
            <div class="col-md-12">
                <div class="feed p-2">

              
                <i class="fas fa-chevron-circle-left post_liked" onclick="window.history.back()"></i>

                    <!-- feed start -->

                    <?php
                        foreach ($posts as $post):
                        ?>

                    <!--POST-->

                    <?php if ($post['POST_TYPE'] == 'REGULAR_POST') {?>

                    <div class="bg-white border mt-2">
                        <div>
                            <div class="d-flex flex-row justify-content-between align-items-center p-2 border-bottom">
                                <div class="d-flex flex-row align-items-center feed-text px-2"><img
                                        class="rounded-circle"
                                        src="<?php echo $this->webroot . "img/" . $post['USER_IMAGE']; ?>" width="45">
                                    <div class="d-flex flex-column flex-wrap ml-2">
                                        <?php
                                                if ($post['USER_ID'] != $this->Session->read('Auth.User.id')) {
                                                    echo $this->Html->link($post['FULL_NAME'], array('controller' => 'users', 'action' => 'view', $post['USER_ID']), array('class' => 'font-weight-bold'));
                                                } else {
                                                    echo $this->Html->link($post['FULL_NAME'], array('controller' => 'users', 'action' => 'index'), array('class' => 'font-weight-bold'));
                                                }
                                                    ?>
                                        <span
                                            class="text-black-50 time"><?php echo $this->Time->format($post['MODIFIED'], '%B %e, %Y %H:%M %p'); ?></span>
                                    </div>
                                </div>

                                <div class="d-flex justify-content-end socials p-2 py-3">
                                    <?php if ($post['USER_ID'] == $this->Session->read('Auth.User.id')) { ?>
                                    <div class="card-actions float-right">
                                        <div class="dropdown">
                                            <button class="btn btn-default" type="button" id="dropdownMenuButton"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                    fill="currentColor" class="bi bi-three-dots-vertical"
                                                    viewBox="0 0 16 16">
                                                    <path
                                                        d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z">
                                                    </path>
                                                </svg>
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

                                                <?php echo $this->Html->link('Edit', array('action' => 'edit', $post['POST_ID']), array('class' => 'dropdown-item')); ?>
                                                <?php echo $this->Form->postLink('Delete', array('action' => 'delete', $post['POST_ID']),
                                                            array('class' => 'dropdown-item', 'confirm' => 'Are you sure you want to delete this?')); 
                                                    ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>

                            </div>
                        </div>

                        <!--CONTENT DIV-->
                        <div onclick="location.href='<?= $this->webroot?>posts/view/<?=$post['POST_ID']?>'"
                            style="cursor:pointer;">
                            <div class="p-2 px-3"><span><?php echo h($post['CONTENT']); ?></span></div>

                            <?php if ($post['IMAGE'] != null) {?>
                            <div class="feed-image p-2 px-3">
                                <img class="img-fluid img-responsive"
                                    src="<?php echo $this->webroot . "img/" . $post['IMAGE']; ?>">
                            </div>
                            <?php }?>
                        </div>
                        <!--CONTENT DIV END-->


                        <section class="post-footer">
                            <hr>
                            <div class="post-footer-option container">
                                <ul class="list-unstyled">
                                    <?php
                                      if ($post['IS_LIKED'] == 1) {
                                        echo $this->Html->tag('i', 'Unlike', array('class' => 'fa fa-thumbs-up post_liked', 'id' => 'post_unlike_' . $post['POST_ID'] . '', 'onclick' => 'unlike(' . $post['POST_ID'] . ');'));
                                        echo $this->Html->tag('i', 'Like', array('class' => 'fa fa-thumbs-up display_none', 'id' => 'post_like_' . $post['POST_ID'] . '', 'onclick' => 'like(' . $post['POST_ID'] . ');'));
                                    } else {
                                        echo $this->Html->tag('i', 'Unlike', array('class' => 'fa fa-thumbs-up post_liked display_none', 'id' => 'post_unlike_' . $post['POST_ID'] . '', 'onclick' => 'unlike(' . $post['POST_ID'] . ');'));
                                        echo $this->Html->tag('i', 'Like', array('class' => 'fa fa-thumbs-up', 'id' => 'post_like_' . $post['POST_ID'] . '', 'onclick' => 'like(' . $post['POST_ID'] . ');'));
                                    }
                                   ?>
                                    <i
                                        id="post_like_count_<?php echo $post['POST_ID']; ?>"><?php echo $post['LIKES_COUNT'] == 0 ? "" : $post['LIKES_COUNT']; ?></i>

                                    <i class="p-3"></i>

                                    <?php echo $this->Html->tag('i', 'Comment', array('class' => 'fas fa-comments post_liked', 'id' => 'post_comment_' . $post['POST_ID'] . '', 'onclick' => 'loadComment(' . $post['POST_ID'] . ')')); ?>
                                    <i
                                        id="post_comment_count_<?=$post['POST_ID'] ?>"><?php echo $post['COMMENTS_COUNT'] == 0 ? "" : $post['COMMENTS_COUNT'] ?></i>

                                    <i class="p-3"></i>

                                    <?php
                                          echo $this->Form->postLink('Share', array('action' => 'repost',  $post['POST_ID']),
                                                array('class' => 'fas fa-share')
                                            );
                                        ?>

                                </ul>
                            </div>
                        </section>
                    </div>



                    <!--REPOST-->

                    <?php } else if ($post['POST_TYPE'] == 'SHARED_POST') {?>
                    <div class="bg-white border mt-2">
                        <div>
                            <div class="d-flex flex-row justify-content-between align-items-center p-2 border-bottom">
                                <div class="d-flex flex-row align-items-center feed-text px-2"><img
                                        class="rounded-circle"
                                        src="<?php echo $this->webroot . "img/" . $post['USER_IMAGE']; ?>" width="45">
                                    <div class="d-flex flex-column flex-wrap ml-2">
                                        <?php
                                        if ($post['USER_ID'] != $this->Session->read('Auth.User.id')) {
                                            echo $this->Html->link($post['FULL_NAME'], array('controller' => 'users', 'action' => 'view', $post['USER_ID']), array('class' => 'font-weight-bold'));
                                        } else {
                                            echo $this->Html->link($post['FULL_NAME'], array('controller' => 'users', 'action' => 'index'), array('class' => 'font-weight-bold'));
                                        }
                                            ?>
                                        <span
                                            class="text-black-50 time"><?php echo $this->Time->format($post['MODIFIED'], '%B %e, %Y %H:%M %p'); ?></span>
                                    </div>
                                </div>

                                <div class="d-flex justify-content-end socials p-2 py-3">
                                    <?php if ($post['USER_ID'] == $this->Session->read('Auth.User.id')) { ?>
                                    <div class="card-actions float-right">
                                        <div class="dropdown">
                                            <button class="btn btn-default" type="button" id="dropdownMenuButton"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                    fill="currentColor" class="bi bi-three-dots-vertical"
                                                    viewBox="0 0 16 16">
                                                    <path
                                                        d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z">
                                                    </path>
                                                </svg>
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

                                                <?php //echo $this->Html->link('Edit', array('action' => 'edit', $post['POST_ID']), array('class' => 'dropdown-item')); ?>
                                                <?php echo $this->Form->postLink('Delete', array('action' => 'delete', $post['POST_ID']),
                                                                array('class' => 'dropdown-item', 'confirm' => 'Are you sure you want to delete this?')); 
                                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>




                            </div>
                        </div>

                        <div class="p-2 px-3">

                            <div onclick="location.href='<?= $this->webroot?>posts/view/<?=$post['POST_ID']?>'"
                                style="cursor:pointer;">
                                <!--Original Post-->
                                <?php if($post['IS_DELETED']==1){
                                     echo '<div class="text-muted"><small>Content not available or maybe deleted.</small></div>';
                                     } else {
                                ?>
                                <div class="bg-white border mt-2">
                                    <div>
                                        <div
                                            class="d-flex flex-row justify-content-between align-items-center p-2 border-bottom">
                                            <div class="d-flex flex-row align-items-center feed-text px-2"><img
                                                    class="rounded-circle"
                                                    src="<?php echo $this->webroot . "img/" . $post['ORG_USER_IMAGE']; ?>"
                                                    width="45">
                                                <div class="d-flex flex-column flex-wrap ml-2">
                                                    <?php
                                                if ($post['ORG_USER_ID'] != $this->Session->read('Auth.User.id')) {
                                                        echo $this->Html->link($post['ORG_NAME'], array('controller' => 'users', 'action' => 'view', $post['ORG_USER_ID']), array('class' => 'font-weight-bold'));
                                                    } else {
                                                        echo $this->Html->link($post['ORG_NAME'], array('controller' => 'users', 'action' => 'index'), array('class' => 'font-weight-bold'));
                                                    }
                                                    ?>
                                                    <span
                                                        class="text-black-50 time"><?php echo $this->Time->format($post['ORG_MODIFIED'], '%B %e, %Y %H:%M %p'); ?></span>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="p-2 px-3"><span><?php echo h($post['CONTENT']); ?></span>
                                    </div>

                                    <?php if ($post['IMAGE'] != null) {?>
                                    <div class="feed-image p-2 px-3">
                                        <img class="img-fluid img-responsive"
                                            src="<?php echo $this->webroot . "img/" . $post['IMAGE']; ?>">
                                    </div>
                                    <?php }?>
                                </div>
                                <?php }?>
                                <!--End Original Post-->
                            </div>

                        </div>


                        <section class="post-footer">
                            <hr>
                            <div class="post-footer-option container">
                                <ul class="list-unstyled">
                                    <?php
                                      if ($post['IS_LIKED'] == 1) {
                                        echo $this->Html->tag('i', 'Unlike', array('class' => 'fa fa-thumbs-up post_liked', 'id' => 'post_unlike_' . $post['POST_ID'] . '', 'onclick' => 'unlike(' . $post['POST_ID'] . ');'));
                                        echo $this->Html->tag('i', 'Like', array('class' => 'fa fa-thumbs-up display_none', 'id' => 'post_like_' . $post['POST_ID'] . '', 'onclick' => 'like(' . $post['POST_ID'] . ');'));
                                        } else {
                                            echo $this->Html->tag('i', 'Unlike', array('class' => 'fa fa-thumbs-up post_liked display_none', 'id' => 'post_unlike_' . $post['POST_ID'] . '', 'onclick' => 'unlike(' . $post['POST_ID'] . ');'));
                                            echo $this->Html->tag('i', 'Like', array('class' => 'fa fa-thumbs-up', 'id' => 'post_like_' . $post['POST_ID'] . '', 'onclick' => 'like(' . $post['POST_ID'] . ');'));
                                        }
                                       ?>
                                    <i
                                        id="post_like_count_<?php echo $post['POST_ID']; ?>"><?php echo $post['LIKES_COUNT'] == 0 ? "" : $post['LIKES_COUNT']; ?></i>
                                    <i class="p-3"></i>
                                    <?php echo $this->Html->tag('i', 'Comment', array('class' => 'fas fa-comments post_liked', 'id' => 'post_comment_' . $post['POST_ID'] . '', 'onclick' => 'loadComment(' . $post['POST_ID'] . ')')); ?>
                                    <i
                                        id="post_comment_count_<?=$post['POST_ID'] ?>"><?php echo $post['COMMENTS_COUNT'] == 0 ? "" : $post['COMMENTS_COUNT'] ?></i>
                                    <i class="p-3"></i>
                                    <?php
                                          echo $this->Form->postLink('Share', array('action' => 'repost',  $post['POST_ID']),
                                                array('class' => 'fas fa-share')
                                            );
                                            ?>

                                    <!-- <i class="fa fa-share">Share</i> -->
                                </ul>
                            </div>
                        </section>


                    </div>

                    <?php }?>


                    <!-- feed end -->



                    <div class="card card-primary">

                        <!-- add comment start -->
                        <form>
                            <div class="card-body">
                                <div class="form-group">
                                    <input class="form-control form-control-lg" rows="1" name="post_txt"
                                        id="post_txt_<?= $post['POST_ID'] ?>" placeholder="Add comment"></input>
                                </div>
                                <div class="float-right">
                                    <?=$this->Form->button('Add', array('label' => false, 'class' => 'btn btn-primary', 'onclick' => 'addComment('.$post['POST_ID'].')', 'type' => 'button'))?>
                                </div>
                            </div>
                            <br>
                        </form>
                        <!-- add comment end -->
                    </div>
                    <!-- END OF COMMENTS -->

                    <?php endforeach;?>
                    <?php unset($post);?>

                    <!--START OF COMMENTS -->
                    <div id="commentDiv">
                        <?php  foreach ($comments as $comment): ?>

                        <div class="comment-widgets border-bottom" id="commentDiv">
                            <div class="d-flex flex-row comment-row m-t-0">
                                <div class="p-2"><img
                                        src="<?php echo $this->webroot . "img/" . $comment['User']['image']; ?>"
                                        alt="user" width="50" class="rounded-circle">
                                </div>
                                <div class="comment-text w-100">
                                    <div class="d-flex flex-column flex-wrap ml-2">
                                        <?php
                                           if ($comment['User']['id'] != $this->Session->read('Auth.User.id')) {
                                              echo $this->Html->link($comment['User']['full_name'], array('controller' => 'users', 'action' => 'view', $comment['User']['id']), array('class' => 'font-weight-bold'));
                                             } else {
                                                echo $this->Html->link($comment['User']['full_name'], array('controller' => 'users', 'action' => 'index'), array('class' => 'font-weight-bold'));
                                               }
                                              ?>
                                        <span
                                            class="text-black-50 time"><?php echo $this->Time->format($comment['Comment']['modified'], '%B %e, %Y %H:%M %p'); ?></span>
                                    </div>
                                    <span class="m-b-15 d-block">
                                        <?php echo h($comment['Comment']['comment_text']); ?>
                                    </span>
                                    <?php if ($comment['User']['id'] == $this->Session->read('Auth.User.id')) {?>
                                    <div class="comment-footer">
                                        <?php echo $this->Form->postLink('Delete', array('controller' => 'comments', 'action' => 'deleteComment', $comment['Comment']['id']),
                                      array('class' => 'fas fa-trash icon_gray', 'confirm' => 'Are you sure you want to delete this?'));
                                      ?>
                                    </div>
                                    <?php }?>
                                </div>
                            </div>
                        </div>

                        <?php endforeach;?>
                        <?php unset($comment);?>
                    </div>
                    <!-- END OF COMMENTS -->

                </div>
            </div>
        </div>
    </div>

    <?php

$paginator = $this->Paginator;

echo "<center><div class='paging'>";

echo $paginator->first('First');
echo " ";

if ($paginator->hasPrev()) {
    echo $paginator->prev('<<');
}
echo " ";
echo $paginator->numbers(array('modulus' => 2));
echo " ";

if ($paginator->hasNext()) {
    echo $paginator->next('>>');
}

echo " ";
echo $paginator->last('Last');
echo "</div></center>";

?>


</div>