<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'Microblog');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>

<head>
    <?php echo $this->Html->charset(); ?>
    <title>
        <?php echo $cakeDescription ?>:
        <?php echo $this->fetch('title'); ?>
    </title>

    <?php echo $this->Html->meta('icon'); ?>


    <link rel="stylesheet" id="theme_link"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/4.3.1/flatly/bootstrap.min.css" />


    <?php echo $this->Html->css(['docs', 'navbar-fixed-left.min', 'navbar-fixed-right.min']); ?>


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
        crossorigin="anonymous">


    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha256-pasqAKBDmFT4eHoN2ndd6lN370kFiGUFyTiUHWhU7k8=" crossorigin="anonymous"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>

    <?php	echo $this->Html->script('docs'); ?>

    
    <?php //echo $this->Html->css(['Bootswatch.min', 'Fontawesome.min', 'docs', 'navbar-fixed-left.min', 'navbar-fixed-right.min']); ?>

    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
        crossorigin="anonymous"> -->

   
    <?php	//echo $this->Html->script(['jQuery3.4.1.slim.min', 'jQuery3.4.1.min', 'popper.min', 'bootstrap4.3.1.min', 'docs']); ?>

    <!-- <?php //echo $this->Html->css('cake.generic'); ?> -->
    <?php echo $this->fetch('meta'); ?>
    <?php echo $this->fetch('css'); ?>
    <?php echo $this->fetch('script'); ?>

</head>

<body>
    <div id="container">
        <div id="header" style="display:none;">
            <h1><?php echo $this->Html->link($cakeDescription, 'https://cakephp.org'); ?></h1>
        </div>
        <div id="content">

            <?php echo $this->Flash->render(); ?>

            <?php echo $this->fetch('content'); ?>
        </div>
        <div id="footer" style="display:none;">
            <?php echo $this->Html->link(
    $this->Html->image('cake.power.gif', array('alt' => $cakeDescription, 'border' => '0')),
    'https://cakephp.org/',
    array('target' => '_blank', 'escape' => false, 'id' => 'cake-powered')
);
?>
            <p>
                <?php echo $cakeVersion; ?>
            </p>
        </div>
    </div>
    <?php echo $this->element('sql_dump'); ?>
</body>

</html>
<script>
function server_path() {
    //return '/microblog2';
    return '<?=$this->base ?>';
}

$(document).ready(function() {
 if(<?=$this->Session->read('Auth.User.id') !== null ?>){
    loadUsersToFollow();
    setInterval(function(){
        loadUsersToFollow();
    }, 10000);
}

});
</script>