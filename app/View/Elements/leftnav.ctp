<nav class="navbar navbar-expand-md navbar-dark bg-primary fixed-left">
    <!-- <a class="navbar-brand" href>Microblog</a> -->


    <?php
echo $this->Html->link(
    $this->Html->tag('i', '', array('class' => 'fas fa-blog')) .
    $this->Html->tag('span', ' Microblog'),
    array(
        'controller' => 'posts',
        'action' => 'index',
    ),
    array('escape' => false, 'class' => 'navbar-brand')
);
?>


    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault"
        aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav">
            <li class="nav-item">
                <div class="">
                    <img src="<?php echo $this->webroot . "img/" . $this->Session->read('Auth.User.image'); ?>"
                        class="img-responsive img-fluid" alt="">
                </div>
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name">
                        <?php echo $this->Session->read('Auth.User.full_name'); ?>
                    </div>
                    <div class="profile-usertitle-username">
                        <?php echo "@" . $this->Session->read('Auth.User.username'); ?>
                    </div>
                </div>
                <div class="profile-userbuttons">
                    <button type="button" class="btn btn-danger btn-sm"><?=$this->Html->link(("Log out"),
    array(
        'controller' => 'users',
        'action' => 'logout',
    ), ['style' => 'color:white'])?></button>
                </div>
                <hr class="my-2">

            <li class="nav-item">
                <?php
echo $this->Html->link(
    $this->Html->tag('i', '', array('class' => 'fas fa-home')) .
    $this->Html->tag('span', ' Home'),
    array(
        'controller' => 'posts',
        'action' => 'index',
    ),
    array('escape' => false, 'class' => 'nav-link')
);
?>
            </li>
            <li class="nav-item">
                <?php
echo $this->Html->link(
    $this->Html->tag('i', '', array('class' => 'fas fa-user-circle')) .
    $this->Html->tag('span', ' Profile'),
    array(
        'controller' => 'users',
        'action' => 'index',
    ),
    array('escape' => false, 'class' => 'nav-link')
);
?>
            </li>
            <li class="nav-item">
                <?php


echo $this->Html->link(
    $this->Html->tag('i', '', array('class' => 'fas fa-user-friends')) .
    $this->Html->tag('span', ' Followers(' . count($followers_view).')'),
    array(
        'controller' => 'follows',
        'action' => 'followers', 1, $this->Session->read('Auth.User.id')),
    array('escape' => false, 'class' => 'nav-link'));
?>
            </li>

            <li class="nav-item">
                <?php
echo $this->Html->link(
    $this->Html->tag('i', '', array('class' => 'fas fa-user-friends')) .
    $this->Html->tag('span', ' Following(' . count($followings_view).')'),
    array(
        'controller' => 'follows',
        'action' => 'following', 2, $this->Session->read('Auth.User.id')),
    array('escape' => false, 'class' => 'nav-link')
);

?>

            </li>
            <!-- <li class="nav-item">
                <div class="media">
                    <div class="form-group">
                        <label>
                            <a class="nav-link"><i class="fas fa-theme"></i> Select theme</a>
                        </label>
                        <select class="form-control width-md" id="theme_select" onchange="selectTheme(value)"></select>
                    </div>
                </div>
            </li> -->

        </ul>
    </div>
</nav>