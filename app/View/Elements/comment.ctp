<form>
    <div class="card-body">
        <div class="form-group">
            <input class="form-control form-control-sm" rows="1" name="post_txt" id="post_txt_<?=$post_id ?>"
                placeholder="Add comment"></input>
        </div>
        <div class="float-right">
            <?=$this->Form->button('Add', array('label' => false, 'class' => 'btn btn-primary btn-sm', 'onclick' => 'addComment(' . $post_id . ')', 'type' => 'button')) ?>
        </div>
    </div>
    <br>
</form>


<!--START OF COMMENTS -->
<div id="commentDiv_<?=$post_id ?>">
    <?php foreach ($comments as $comment): ?>
    <div id="commentLogDiv_<?=$comment['Comment']['id'] ?>">
        <div class="comment-widgets border-bottom">
            <div class="d-flex flex-row comment-row m-t-0">
                <div class="p-2"><img src="<?php echo $this->webroot . "img/" . $comment['User']['image']; ?>"
                        alt="user" width="50" class="rounded-circle">
                </div>
                <div class="comment-text w-100">
                    <div class="d-flex flex-column flex-wrap ml-2">
                        <?php
                        if ($comment['User']['id'] != $this->Session->read('Auth.User.id')) {
                            echo $this->Html->link($comment['User']['full_name'], array('controller' => 'users', 'action' => 'view', $comment['User']['id']), array('class' => 'font-weight-bold'));
                        } else {
                            echo $this->Html->link($comment['User']['full_name'], array('controller' => 'users', 'action' => 'index'), array('class' => 'font-weight-bold'));
                        }
                        ?>
                        <span
                            class="text-black-50 time"><?php echo $this->Time->format($comment['Comment']['modified'], '%B %e, %Y %H:%M %p'); ?></span>
                    </div>
                    <span class="m-b-15 d-block">
                        <?php echo h($comment['Comment']['comment_text']); ?>
                    </span>
                    <?php if ($comment['User']['id'] == $this->Session->read('Auth.User.id')) { ?>
                    <div class="comment-footer">
                        <i class="fas fa-trash icon_gray"
                            onclick="deleteComment('<?=$comment['Comment']['id'] ?>');">Delete</i>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>

    <?php endforeach; ?>

    <?php if (count($comments) > 0) { ?>

    <div class="row justify-content-center p-2">
        <i class="fas fa-load icon_gray"
            onclick="location.href='<?=$this->webroot ?>posts/view/<?=$post_id ?>'">More</i>
    </div>

    <?php }
unset($comment); ?>

</div>
<!-- END OF COMMENTS -->