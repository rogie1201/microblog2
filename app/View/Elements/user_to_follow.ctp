      <div>
          <?php foreach ($follows as $tofollow): ?>

          <div class="media">
              <img src="<?php echo $this->webroot . "img/" . $tofollow['User']['image']; ?>" width="56" height="56"
                  class="rounded-circle mr-2">
              <div class="media-body">
                  <!-- <p class="my-1"><strong><?php //echo $tofollow['User']['full_name']; ?></strong></p> -->

                  <?php
                    if ($tofollow['User']['id'] != $this->Session->read('Auth.User.id')) {
                        echo $this->Html->link($tofollow['User']['full_name'], array('controller' => 'users', 'action' => 'view', $tofollow['User']['id']), array('class' => 'font-weight-bold'));
                    } else {
                        echo $this->Html->link($tofollow['User']['full_name'], array('controller' => 'users', 'action' => 'index'), array('class' => 'font-weight-bold'));

                    }
                    ?>

                  <div class="text-muted">
                      <small><?php echo '@' . $tofollow['User']['username']; ?></small>
                  </div>

                  <?php echo $this->Form->postLink('Follow', array('controller' => 'users', 'action' => 'follow', $tofollow['User']['id']),
                        array('class' => 'btn btn-sm btn-outline-primary')
                    );
                    ?>

              </div>
          </div>
          <hr class="my-2">
          <?php endforeach; ?>
          <?php unset($tofollow); ?>

      </div>