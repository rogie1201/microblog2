<nav class="navbar navbar-expand-md navbar-dark bg-primary fixed-right">

    <!-- <input class="form-control form-control-lg" type="text" placeholder="&#x1f50d; Search"> -->








                <?php

$word = (isset($keyword_search)) ? trim($keyword_search, "\/") : '';

echo $this->Form->create('User', array('class' => 'searchForm', 'role' => 'search', 'autocomplete' => 'off', 'type' => 'get',
    'url' => array('controller' => 'search',
        'action' => 'searchPosts',
    ),
));
echo $this->Form->input('keyword', array('class' => 'form-control input-sm', 'maxlength' => "64", 'placeholder' => ' Search...', 'value' => $word, 'div' => false, 'label' => false));
echo $this->Form->submit('Search',
    array('style' => 'display:none'));
echo $this->Form->end();
?>







    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault"
        aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <hr class="my-2">

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">

        <div class="card col-md-12">
            <div class="card-header">
                <div class="card-actions float-right">
                    <div class="dropdown show">
                        <a href="#" data-toggle="dropdown" data-display="static">
                            <!-- <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" class="feather feather-more-horizontal align-middle">
                                <circle cx="12" cy="12" r="1"></circle>
                                <circle cx="19" cy="12" r="1"></circle>
                                <circle cx="5" cy="12" r="1"></circle>
                            </svg> -->
                        </a>

                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </div>
                </div>
                <h5 class="card-title mb-0">Who to follow</h5>
            </div>
            <div class="card-body" id="rightNav">
             <!--LOAD USER TO FOLLOW -->
            </div>

        </div>
    </div>


    </div>
</nav>



